function reduce(numerator, denominator) {
    var gcd = function gcd(a, b) {
        return b ? gcd(b, a % b) : a;
    };
    gcd = gcd(numerator, denominator);
    return [numerator / gcd, denominator / gcd];
}


function getMedian(args) {
    if (!args.length) { return 0 };
    var numbers = args.slice(0).sort((a, b) => a - b);
    var middle = Math.floor(numbers.length / 2);
    var isEven = numbers.length % 2 === 0;
    return isEven ? (numbers[middle] + numbers[middle - 1]) / 2 : numbers[middle];
}

var quiz = {

    duration: 45,
    question: 20,
    randomize: false,


    data: [
        // how to get linebreak, how to get $
        // circle equations are too long - need linebreak
        //1, 3, 4
        // DIAGRAMS!
        // Conversions in test 1
        // ${op} 
        //`
        /*        return {
                        	ques:` 1`,
                        	Hint: `Hint: 1 `,
                        	Answer: `Answer:  1`

                         }
        */






        {
            op1: 0,
            op2: 0,
            op3: 0,
            op4: 0,
            question: function() {
                op1 = Math.ceil(Math.random() * 5) + 1;
                op2 = Math.ceil(Math.random() * 2) + 1;
                op3 = Math.ceil(Math.random() * 5) + 1;
                op4 = Math.ceil(Math.random() * 5) + 1;
                op5 = Math.ceil(Math.random() * 5) + 1;
                op6 = Math.ceil(Math.random() * 5) + 1;
                op7 = op2 + op3;

                // This return statement is responsible for desiplaying question Hint and answer
                return {
                    ques: ` 40% of the tickets sold at a school carnival were early-admission tickets.</br>
                    If the school sold 80 tickets in all, how many early-admission tickets did it sell? `,
                    Hint: ` `,
                    Answer: `Answer: 32`,
                    imageScript: `
                    \\begin{tikzpicture}
                    \\draw (0,0) circle (1in);
                  \\end{tikzpicture} `
                };
            },
            answer: function() {
                return op1 * op2;
            }
        },



        {
            op1: 0,
            op2: 0,
            op3: 0,
            op4: 0,
            question: function() {
                op1 = Math.ceil(Math.random() * 5) + 1;
                op2 = Math.ceil(Math.random() * 5) + 1;
                op3 = Math.ceil(Math.random() * 5) + 1;
                op4 = Math.ceil(Math.random() * 5) + 1;
                op5 = Math.ceil(Math.random() * 5) + 1;
                op6 = Math.ceil(Math.random() * 5) + 1;



                return {
                    ques: `7% is what percent of $7? </br>
					Write your answer using a percent sign %. For example, 0.5%, 12.7%, or 56%.`,
                    Hint: ``,
                    Answer: `7`
                };

                // This return statement is responsible for displaying only question replace it same as 1st one
                return
            },
            answer: function() {
                return op1 * op2;
            }
        },




        {
            op1: 0,
            op2: 0,
            op3: 0,
            op4: 0,
            question: function() {
                op1 = Math.ceil(Math.random() * 5) + 1;
                op2 = Math.ceil(Math.random() * 5) + 1;
                op3 = Math.ceil(Math.random() * 5) + 1;
                op4 = Math.ceil(Math.random() * 5) + 1;
                op5 = Math.ceil(Math.random() * 5) + 1;
                op6 = Math.ceil(Math.random() * 5) + 1;



                return {
                    ques: ` Lexi took a survey to find out how many students in her school have a pet.</br>
  							There are 700 students in the school. 427 of them have a pet.
  							What percentage of the students in Lexi's school have a pet?,</br>
  							Write your answer using a percent sign (%).`,
                    Hint: ``,
                    Answer: `61%`
                };

                // This return statement is responsible for displaying only question replace it same as 1st one
                return
            },
            answer: function() {
                return op1 * op2;
            }
        },

        {
            op1: 0,
            op2: 0,
            op3: 0,
            op4: 0,
            question: function() {
                op1 = Math.ceil(Math.random() * 5) + 1;
                op2 = Math.ceil(Math.random() * 5) + 1;
                op3 = Math.ceil(Math.random() * 5) + 1;
                op4 = Math.ceil(Math.random() * 5) + 1;
                op5 = Math.ceil(Math.random() * 5) + 1;
                op6 = Math.ceil(Math.random() * 5) + 1;



                return {
                    ques: `  What is the percent of decrease from 72 to 36?</br>
  								Write your answer using a percent sign (%).`,
                    Hint: ``,
                    Answer: `50%`
                };

                // This return statement is responsible for displaying only question replace it same as 1st one
                return
            },
            answer: function() {
                return op1 * op2;
            }
        },



        {
            op1: 0,
            op2: 0,
            op3: 0,
            op4: 0,
            question: function() {
                op1 = Math.ceil(Math.random() * 5) + 1;
                op2 = Math.ceil(Math.random() * 5) + 1;
                op3 = Math.ceil(Math.random() * 5) + 1;
                op4 = Math.ceil(Math.random() * 5) + 1;
                op5 = Math.ceil(Math.random() * 5) + 1;
                op6 = Math.ceil(Math.random() * 5) + 1;



                return {
                    ques: `   Since switching to a new career, Harper has been making $40,145 annually.</br>
                     That is 50% less than she got paid in the past. How much did Harper make then?`,
                    Hint: ``,
                    Answer: `$80,290`
                };

                // This return statement is responsible for displaying only question replace it same as 1st one
                return
            },
            answer: function() {
                return op1 * op2;
            }
        },


        {
            op1: 0,
            op2: 0,
            op3: 0,
            op4: 0,
            question: function() {
                op1 = Math.ceil(Math.random() * 5) + 1;
                op2 = Math.ceil(Math.random() * 5) + 1;
                op3 = Math.ceil(Math.random() * 5) + 1;
                op4 = Math.ceil(Math.random() * 5) + 1;
                op5 = Math.ceil(Math.random() * 5) + 1;
                op6 = Math.ceil(Math.random() * 5) + 1;



                return {
                    ques: `   Gary wants to buy a skateboard. The original price is  $86.10 What is the sale price?`,
                    Hint: ``,
                    Answer: `8.61`
                };

                // This return statement is responsible for displaying only question replace it same as 1st one
                return
            },
            answer: function() {
                return op1 * op2;
            }
        },


        {
            op1: 0,
            op2: 0,
            op3: 0,
            op4: 0,
            question: function() {
                op1 = Math.ceil(Math.random() * 5) + 1;
                op2 = Math.ceil(Math.random() * 5) + 1;
                op3 = Math.ceil(Math.random() * 5) + 1;
                op4 = Math.ceil(Math.random() * 5) + 1;
                op5 = Math.ceil(Math.random() * 5) + 1;
                op6 = Math.ceil(Math.random() * 5) + 1;



                return {
                    ques: `  Is this function linear, quadratic, or exponential?`,
                    Hint: ``,
                    Answer: `Linear`
                };

                // This return statement is responsible for displaying only question replace it same as 1st one
                return
            },
            answer: function() {
                return op1 * op2;
            }
        },

        {
            op1: 0,
            op2: 0,
            op3: 0,
            op4: 0,
            question: function() {
                op1 = Math.ceil(Math.random() * 5) + 1;
                op2 = Math.ceil(Math.random() * 5) + 1;
                op3 = Math.ceil(Math.random() * 5) + 1;
                op4 = Math.ceil(Math.random() * 5) + 1;
                op5 = Math.ceil(Math.random() * 5) + 1;
                op6 = Math.ceil(Math.random() * 5) + 1;



                return {
                    ques: `  What type of function does this graph show?`,
                    Hint: ``,
                    Answer: `Linear`,
                    imageScript: `\begin{tikzpicture}
                    \draw[white,thick,solid] (0,0) circle (1in);
                  \end{tikzpicture}`
                };

                // This return statement is responsible for displaying only question replace it same as 1st one
                return
            },
            answer: function() {
                return op1 * op2;
            }
        },


        {
            op1: 0,
            op2: 0,
            op3: 0,
            op4: 0,
            question: function() {
                op1 = Math.ceil(Math.random() * 5) + 1;
                op2 = Math.ceil(Math.random() * 5) + 1;
                op3 = Math.ceil(Math.random() * 5) + 1;
                op4 = Math.ceil(Math.random() * 5) + 1;
                op5 = Math.ceil(Math.random() * 5) + 1;
                op6 = Math.ceil(Math.random() * 5) + 1;



                return {
                    ques: `  Write your answer in simplest form</br>
                    $ -√2(-6 +  √3 $	`,
                    Hint: ``,
                    Answer: `{2} - √6`
                };

                // This return statement is responsible for displaying only question replace it same as 1st one
                return
            },
            answer: function() {
                return op1 * op2;
            }
        },

        {
            op1: 0,
            op2: 0,
            op3: 0,
            op4: 0,
            question: function() {
                op1 = Math.ceil(Math.random() * 5) + 1;
                op2 = Math.ceil(Math.random() * 5) + 1;
                op3 = Math.ceil(Math.random() * 5) + 1;
                op4 = Math.ceil(Math.random() * 5) + 1;
                op5 = Math.ceil(Math.random() * 5) + 1;
                op6 = Math.ceil(Math.random() * 5) + 1;



                return {
                    ques: `  Simplify.</br>
                    $ 	8 ^{1/3}	$	`,
                    Hint: ``,
                    Answer: `2 `
                };

                // This return statement is responsible for displaying only question replace it same as 1st one
                return
            },
            answer: function() {
                return op1 * op2;
            }
        },

        {
            op1: 0,
            op2: 0,
            op3: 0,
            op4: 0,
            question: function() {
                op1 = Math.ceil(Math.random() * 5) + 1;
                op2 = Math.ceil(Math.random() * 5) + 1;
                op3 = Math.ceil(Math.random() * 5) + 1;
                op4 = Math.ceil(Math.random() * 5) + 1;
                op5 = Math.ceil(Math.random() * 5) + 1;
                op6 = Math.ceil(Math.random() * 5) + 1;



                return {
                    ques: `  Franklin takes 180 milligrams of an antibiotic. Every hour, his body breaks down 25% of the drug.</br>
                     How much will be left after 8 hours?</br>
						If necessary, round your answer to the nearest tenth.	`,
                    Hint: ``,
                    Answer: `18.0 milligrams`
                };

                // This return statement is responsible for displaying only question replace it same as 1st one
                return
            },
            answer: function() {
                return op1 * op2;
            }
        },

        {
            op1: 0,
            op2: 0,
            op3: 0,
            op4: 0,
            question: function() {
                op1 = Math.ceil(Math.random() * 5) + 1;
                op2 = Math.ceil(Math.random() * 5) + 1;
                op3 = Math.ceil(Math.random() * 5) + 1;
                op4 = Math.ceil(Math.random() * 5) + 1;
                op5 = Math.ceil(Math.random() * 5) + 1;
                op6 = Math.ceil(Math.random() * 5) + 1;



                return {
                    ques: `  Simplify the expression. Assume all variables are positive</br>
                    $ (64s) ^{1/2} $ </br>
                     Write your answer in the form A or A/B , where A and B are constants or variable expressions that have no variables in common.
                      All exponents in your answer should be positive`,
                    Hint: ``,
                    Answer: ` (8s) ^{1/2}`
                };

                // This return statement is responsible for displaying only question replace it same as 1st one
                return
            },
            answer: function() {
                return op1 * op2;
            }
        },

        {
            op1: 0,
            op2: 0,
            op3: 0,
            op4: 0,
            question: function() {
                op1 = Math.ceil(Math.random() * 5) + 1;
                op2 = Math.ceil(Math.random() * 5) + 1;
                op3 = Math.ceil(Math.random() * 5) + 1;
                op4 = Math.ceil(Math.random() * 5) + 1;
                op5 = Math.ceil(Math.random() * 5) + 1;
                op6 = Math.ceil(Math.random() * 5) + 1;



                return {
                    ques: `  Look at this table:</br>
                    Write a linear (y=mx+b), quadratic $ (y=ax^{2}) $, or exponential $ y= a(b)^{x} $ function that models the data.`,
                    Hint: ``,
                    Answer: ` -2x-6`
                };

                // This return statement is responsible for displaying only question replace it same as 1st one
                return
            },
            answer: function() {
                return op1 * op2;
            }
        },


        {
            op1: 0,
            op2: 0,
            op3: 0,
            op4: 0,
            question: function() {
                op1 = Math.ceil(Math.random() * 5) + 1;
                op2 = Math.ceil(Math.random() * 5) + 1;
                op3 = Math.ceil(Math.random() * 5) + 1;
                op4 = Math.ceil(Math.random() * 5) + 1;
                op5 = Math.ceil(Math.random() * 5) + 1;
                op6 = Math.ceil(Math.random() * 5) + 1;



                return {
                    ques: `  Solve for j in the proportion.
                    $  24/80=3/j 	$`,
                    Hint: ``,
                    Answer: ` 10`
                };

                // This return statement is responsible for displaying only question replace it same as 1st one
                return
            },
            answer: function() {
                return op1 * op2;
            }
        },


        {
            op1: 0,
            op2: 0,
            op3: 0,
            op4: 0,
            question: function() {
                op1 = Math.ceil(Math.random() * 5) + 1;
                op2 = Math.ceil(Math.random() * 5) + 1;
                op3 = Math.ceil(Math.random() * 5) + 1;
                op4 = Math.ceil(Math.random() * 5) + 1;
                op5 = Math.ceil(Math.random() * 5) + 1;
                op6 = Math.ceil(Math.random() * 5) + 1;



                return {
                    ques: `  Brooke baked 10 cookies with 2 scoops of flour.</br>
                     How many scoops of flour does Brooke need in order to bake 15 cookies?</br>
                      Assume the relationship is directly proportional.`,
                    Hint: ``,
                    Answer: ` 3`
                };

                // This return statement is responsible for displaying only question replace it same as 1st one
                return
            },
            answer: function() {
                return op1 * op2;
            }
        },


        {
            op1: 0,
            op2: 0,
            op3: 0,
            op4: 0,
            question: function() {
                op1 = Math.ceil(Math.random() * 5) + 1;
                op2 = Math.ceil(Math.random() * 5) + 1;
                op3 = Math.ceil(Math.random() * 5) + 1;
                op4 = Math.ceil(Math.random() * 5) + 1;
                op5 = Math.ceil(Math.random() * 5) + 1;
                op6 = Math.ceil(Math.random() * 5) + 1;



                return {
                    ques: `  Layla loves to try out new bike trails in her area.</br>
                     Last week, it took her 24 minutes to ride a trail that is 6 miles long. </br>
                     Today, she kept the same pace when she rode an 8-mile trail.</br>
  						How long did it take Layla to ride the trail today?`,
                    Hint: ``,
                    Answer: ` 32`
                };

                // This return statement is responsible for displaying only question replace it same as 1st one
                return
            },
            answer: function() {
                return op1 * op2;
            }
        },


        {
            op1: 0,
            op2: 0,
            op3: 0,
            op4: 0,
            question: function() {
                op1 = Math.ceil(Math.random() * 5) + 1;
                op2 = Math.ceil(Math.random() * 5) + 1;
                op3 = Math.ceil(Math.random() * 5) + 1;
                op4 = Math.ceil(Math.random() * 5) + 1;
                op5 = Math.ceil(Math.random() * 5) + 1;
                op6 = Math.ceil(Math.random() * 5) + 1;



                return {
                    ques: `  Which equation gives the rule for this table?</br>
                    $ y=x=18	y=x+21		y=x+51		y=x+27  $`,
                    Hint: ``,
                    Answer: ` y=x+51`
                };

                // This return statement is responsible for displaying only question replace it same as 1st one
                return
            },
            answer: function() {
                return op1 * op2;
            }
        },

        {
            op1: 0,
            op2: 0,
            op3: 0,
            op4: 0,
            question: function() {
                op1 = Math.ceil(Math.random() * 5) + 1;
                op2 = Math.ceil(Math.random() * 5) + 1;
                op3 = Math.ceil(Math.random() * 5) + 1;
                op4 = Math.ceil(Math.random() * 5) + 1;
                op5 = Math.ceil(Math.random() * 5) + 1;
                op6 = Math.ceil(Math.random() * 5) + 1;



                return {
                    ques: ` Nina jars 3 liters of jam everyday. </br>
                    How many days does Nina need to spend making jam if she wants to jar 24 liters of jam in all?`,
                    Hint: ``,
                    Answer: ` 8`
                };

                // This return statement is responsible for displaying only question replace it same as 1st one
                return
            },
            answer: function() {
                return op1 * op2;
            }
        },


        {
            op1: 0,
            op2: 0,
            op3: 0,
            op4: 0,
            question: function() {
                op1 = Math.ceil(Math.random() * 5) + 1;
                op2 = Math.ceil(Math.random() * 5) + 1;
                op3 = Math.ceil(Math.random() * 5) + 1;
                op4 = Math.ceil(Math.random() * 5) + 1;
                op5 = Math.ceil(Math.random() * 5) + 1;
                op6 = Math.ceil(Math.random() * 5) + 1;



                return {
                    ques: ` Solve for u.</br>
                    $ (u+8)/(u+10) = (u+5)/(u+6) $
                    There may be 1 or 2 solutions.`,
                    Hint: ``,
                    Answer: ` -2`
                };

                // This return statement is responsible for displaying only question replace it same as 1st one
                return
            },
            answer: function() {
                return op1 * op2;
            }
        },

        {
            op1: 0,
            op2: 0,
            op3: 0,
            op4: 0,
            question: function() {
                op1 = Math.ceil(Math.random() * 5) + 1;
                op2 = Math.ceil(Math.random() * 5) + 1;
                op3 = Math.ceil(Math.random() * 5) + 1;
                op4 = Math.ceil(Math.random() * 5) + 1;
                op5 = Math.ceil(Math.random() * 5) + 1;
                op6 = Math.ceil(Math.random() * 5) + 1;



                return {
                    ques: ` Solve for r in terms of C.</br>
                    $ C= 2/πr $ </br>
                    r=? `,
                    Hint: ``,
                    Answer: ` C/2π`
                };

                // This return statement is responsible for displaying only question replace it same as 1st one
                return
            },
            answer: function() {
                return op1 * op2;
            }
        },

        {
            op1: 0,
            op2: 0,
            op3: 0,
            op4: 0,
            question: function() {
                op1 = Math.ceil(Math.random() * 5) + 1;
                op2 = Math.ceil(Math.random() * 5) + 1;
                op3 = Math.ceil(Math.random() * 5) + 1;
                op4 = Math.ceil(Math.random() * 5) + 1;
                op5 = Math.ceil(Math.random() * 5) + 1;
                op6 = Math.ceil(Math.random() * 5) + 1;



                return {
                    ques: `Solve for u.</br>
                    $ -4u+9 ≤ 1 $</br>`,
                    Hint: ``,
                    Answer: ` u≥2`
                };

                // This return statement is responsible for displaying only question replace it same as 1st one
                return
            },
            answer: function() {
                return op1 * op2;
            }
        },

        {
            op1: 0,
            op2: 0,
            op3: 0,
            op4: 0,
            question: function() {
                op1 = Math.ceil(Math.random() * 5) + 1;
                op2 = Math.ceil(Math.random() * 5) + 1;
                op3 = Math.ceil(Math.random() * 5) + 1;
                op4 = Math.ceil(Math.random() * 5) + 1;
                op5 = Math.ceil(Math.random() * 5) + 1;
                op6 = Math.ceil(Math.random() * 5) + 1;



                return {
                    ques: `Solve the system of inequalities by graphing.</br>
     						y<-1 </br>
     					 	y>-7 </br>
     
    						Select a line to change it between solid and dotted. Select a region to shade it.`,
                    Hint: ``,
                    Answer: ` BLANK`
                };

                // This return statement is responsible for displaying only question replace it same as 1st one
                return
            },
            answer: function() {
                return op1 * op2;
            }
        },

        {
            op1: 0,
            op2: 0,
            op3: 0,
            op4: 0,
            question: function() {
                op1 = Math.ceil(Math.random() * 5) + 1;
                op2 = Math.ceil(Math.random() * 5) + 1;
                op3 = Math.ceil(Math.random() * 5) + 1;
                op4 = Math.ceil(Math.random() * 5) + 1;
                op5 = Math.ceil(Math.random() * 5) + 1;
                op6 = Math.ceil(Math.random() * 5) + 1;



                return {
                    ques: `The students at Oak Grove High School are selling candles and scented soaps to raise
                     money for a new computer lab.</br>
                     They will earn $2 for every candle they sell. Each bar of soap they sell will earn them $4.</br>
                     They need to raise a minimum of $2,500 to have enough money to finish construction of the computer lab.</br>	
     				 Select the inequality in standard form that describes this situation.</br>
     				 Use the given numbers and the following variables.</br>
     				 x = the number of candles they sell</br>
     				 y = the number of soaps they sell</br>

   					 4x+2y >2500
     				 4x+2y ≥2500 
     				 2x+4y >2500 
      				 2x+4y ≥2500 	
     				 `,
                    Hint: ``,
                    Answer: ` 2x+4y ≥ 2500`
                };

                // This return statement is responsible for displaying only question replace it same as 1st one
                return
            },
            answer: function() {
                return op1 * op2;
            }
        },

        {
            op1: 0,
            op2: 0,
            op3: 0,
            op4: 0,
            question: function() {
                op1 = Math.ceil(Math.random() * 5) + 1;
                op2 = Math.ceil(Math.random() * 5) + 1;
                op3 = Math.ceil(Math.random() * 5) + 1;
                op4 = Math.ceil(Math.random() * 5) + 1;
                op5 = Math.ceil(Math.random() * 5) + 1;
                op6 = Math.ceil(Math.random() * 5) + 1;



                return {
                    ques: `Solve for a.
     						√(4-2a) = √(2a) </br>
     					 	a= ? `,
                    Hint: ``,
                    Answer: ` 1`
                };

                // This return statement is responsible for displaying only question replace it same as 1st one
                return
            },
            answer: function() {
                return op1 * op2;
            }
        },


        {
            op1: 0,
            op2: 0,
            op3: 0,
            op4: 0,
            question: function() {
                op1 = Math.ceil(Math.random() * 5) + 1;
                op2 = Math.ceil(Math.random() * 5) + 1;
                op3 = Math.ceil(Math.random() * 5) + 1;
                op4 = Math.ceil(Math.random() * 5) + 1;
                op5 = Math.ceil(Math.random() * 5) + 1;
                op6 = Math.ceil(Math.random() * 5) + 1;



                return {
                    ques: `Use the following function rule to find f(4)..
     						$ ( f(x)= (4+x)^{2} $</br>
     					 	f(4)= `,
                    Hint: ``,
                    Answer: ` 64`
                };

                // This return statement is responsible for displaying only question replace it same as 1st one
                return
            },
            answer: function() {
                return op1 * op2;
            }
        },

        {
            op1: 0,
            op2: 0,
            op3: 0,
            op4: 0,
            question: function() {
                op1 = Math.ceil(Math.random() * 5) + 1;
                op2 = Math.ceil(Math.random() * 5) + 1;
                op3 = Math.ceil(Math.random() * 5) + 1;
                op4 = Math.ceil(Math.random() * 5) + 1;
                op5 = Math.ceil(Math.random() * 5) + 1;
                op6 = Math.ceil(Math.random() * 5) + 1;



                return {
                    ques: `Match each quadratic function to its graph.</br>
     				$ f(x)  = -x^{2} - 10x -20 		 g(x) = x^{2} +2x-6 $`,
                    Hint: ``,
                    Answer: ` In first graph $ f(x)  = -x^{2} - 10x -20 $ In second graph $ g(x) = x^{2} +2x-6 $ `
                };

                // This return statement is responsible for displaying only question replace it same as 1st one
                return
            },
            answer: function() {
                return op1 * op2;
            }
        },


        {
            op1: 0,
            op2: 0,
            op3: 0,
            op4: 0,
            question: function() {
                op1 = Math.ceil(Math.random() * 5) + 1;
                op2 = Math.ceil(Math.random() * 5) + 1;
                op3 = Math.ceil(Math.random() * 5) + 1;
                op4 = Math.ceil(Math.random() * 5) + 1;
                op5 = Math.ceil(Math.random() * 5) + 1;
                op6 = Math.ceil(Math.random() * 5) + 1;



                return {
                    ques: `Look at this graph:`,
                    Hint: ``,
                    Answer: ` 2, -4 `
                };

                // This return statement is responsible for displaying only question replace it same as 1st one
                return
            },
            answer: function() {
                return op1 * op2;
            }
        },



        {
            op1: 0,
            op2: 0,
            op3: 0,
            op4: 0,
            question: function() {
                op1 = Math.ceil(Math.random() * 5) + 1;
                op2 = Math.ceil(Math.random() * 5) + 1;
                op3 = Math.ceil(Math.random() * 5) + 1;
                op4 = Math.ceil(Math.random() * 5) + 1;
                op5 = Math.ceil(Math.random() * 5) + 1;
                op6 = Math.ceil(Math.random() * 5) + 1;



                return {
                    ques: `Graph the function $ f(x) = 6 x^{2} $ </br>
                         Plot the vertex. Then plot another point on the parabola.</br>
                         If you make a mistake, you can erase your parabola by selecting
                          the second point and placing it on top of the first.`,
                    Hint: ``,
                    Answer: ` BLANK `
                };

                // This return statement is responsible for displaying only question replace it same as 1st one
                return
            },
            answer: function() {
                return op1 * op2;
            }
        },


        {
            op1: 0,
            op2: 0,
            op3: 0,
            op4: 0,
            question: function() {
                op1 = Math.ceil(Math.random() * 5) + 1;
                op2 = Math.ceil(Math.random() * 5) + 1;
                op3 = Math.ceil(Math.random() * 5) + 1;
                op4 = Math.ceil(Math.random() * 5) + 1;
                op5 = Math.ceil(Math.random() * 5) + 1;
                op6 = Math.ceil(Math.random() * 5) + 1;



                return {
                    ques: `Find the discriminant.</br>
                   			$	8 z^{2} + 8z +2 =0	$`,
                    Hint: ``,
                    Answer: ` 0 `
                };

                // This return statement is responsible for displaying only question replace it same as 1st one
                return
            },
            answer: function() {
                return op1 * op2;
            }
        },


        {
            op1: 0,
            op2: 0,
            op3: 0,
            op4: 0,
            question: function() {
                op1 = Math.ceil(Math.random() * 5) + 1;
                op2 = Math.ceil(Math.random() * 5) + 1;
                op3 = Math.ceil(Math.random() * 5) + 1;
                op4 = Math.ceil(Math.random() * 5) + 1;
                op5 = Math.ceil(Math.random() * 5) + 1;
                op6 = Math.ceil(Math.random() * 5) + 1;



                return {
                    ques: `How many real solutions does the equation have?</br>
                   			$	m^{2} + 62=62 $`,
                    Hint: ``,
                    Answer: ` One real solution `
                };

                // This return statement is responsible for displaying only question replace it same as 1st one
                return
            },
            answer: function() {
                return op1 * op2;
            }
        },

        {
            op1: 0,
            op2: 0,
            op3: 0,
            op4: 0,
            question: function() {
                op1 = Math.ceil(Math.random() * 5) + 1;
                op2 = Math.ceil(Math.random() * 5) + 1;
                op3 = Math.ceil(Math.random() * 5) + 1;
                op4 = Math.ceil(Math.random() * 5) + 1;
                op5 = Math.ceil(Math.random() * 5) + 1;
                op6 = Math.ceil(Math.random() * 5) + 1;



                return {
                    ques: `Solve for m</br>
                   			m(m - 3) = 0</br>
                   			Write your answers as integers or as proper or improper fractions in simplest form.
      						m= ? , m=?`,
                    Hint: ``,
                    Answer: ` m=0, m=3 `
                };

                // This return statement is responsible for displaying only question replace it same as 1st one
                return
            },
            answer: function() {
                return op1 * op2;
            }
        },




        {
            op1: 0,
            op2: 0,
            op3: 0,
            op4: 0,
            question: function() {
                op1 = Math.ceil(Math.random() * 5) + 1;
                op2 = Math.ceil(Math.random() * 5) + 1;
                op3 = Math.ceil(Math.random() * 5) + 1;
                op4 = Math.ceil(Math.random() * 5) + 1;
                op5 = Math.ceil(Math.random() * 5) + 1;
                op6 = Math.ceil(Math.random() * 5) + 1;



                return {
                    ques: `Solve for v.</br>
        					$ v^{2}+ 8v+12=0 $
        					Write each solution as an integer, proper fraction, or improper fraction in simplest form.</br>
         					If there are multiple solutions, separate them with commas`,
                    Hint: ``,
                    Answer: ` -6, -2 `
                };

                // This return statement is responsible for displaying only question replace it same as 1st one
                return
            },
            answer: function() {
                return op1 * op2;
            }
        },





        {
            op1: 0,
            op2: 0,
            op3: 0,
            op4: 0,
            question: function() {
                op1 = Math.ceil(Math.random() * 5) + 1;
                op2 = Math.ceil(Math.random() * 5) + 1;
                op3 = Math.ceil(Math.random() * 5) + 1;
                op4 = Math.ceil(Math.random() * 5) + 1;
                op5 = Math.ceil(Math.random() * 5) + 1;
                op6 = Math.ceil(Math.random() * 5) + 1;



                return {
                    ques: `Factor.</br>
       					$ 2w^{2} +9w +10 $ `,
                    Hint: ``,
                    Answer: ` (2w+5) (w+2) `
                };

                // This return statement is responsible for displaying only question replace it same as 1st one
                return
            },
            answer: function() {
                return op1 * op2;
            }
        },


        {
            op1: 0,
            op2: 0,
            op3: 0,
            op4: 0,
            question: function() {
                op1 = Math.ceil(Math.random() * 5) + 1;
                op2 = Math.ceil(Math.random() * 5) + 1;
                op3 = Math.ceil(Math.random() * 5) + 1;
                op4 = Math.ceil(Math.random() * 5) + 1;
                op5 = Math.ceil(Math.random() * 5) + 1;
                op6 = Math.ceil(Math.random() * 5) + 1;



                return {
                    ques: `Find the product. Simplify your answer.</br>
     					(a+2) (2a-4)`,
                    Hint: ``,
                    Answer: ` $  2a^{2}-8  $ `
                };

                // This return statement is responsible for displaying only question replace it same as 1st one
                return
            },
            answer: function() {
                return op1 * op2;
            }
        },


        {
            op1: 0,
            op2: 0,
            op3: 0,
            op4: 0,
            question: function() {
                op1 = Math.ceil(Math.random() * 5) + 1;
                op2 = Math.ceil(Math.random() * 5) + 1;
                op3 = Math.ceil(Math.random() * 5) + 1;
                op4 = Math.ceil(Math.random() * 5) + 1;
                op5 = Math.ceil(Math.random() * 5) + 1;
                op6 = Math.ceil(Math.random() * 5) + 1;



                return {
                    ques: `Find the product. Simplify your answer.
       						$ (3k-1) (3k+1) $`,
                    Hint: ``,
                    Answer: ` $  9k^{2}-1  $  `
                };

                // This return statement is responsible for displaying only question replace it same as 1st one
                return
            },
            answer: function() {
                return op1 * op2;
            }
        },


        {
            op1: 0,
            op2: 0,
            op3: 0,
            op4: 0,
            question: function() {
                op1 = Math.ceil(Math.random() * 5) + 1;
                op2 = Math.ceil(Math.random() * 5) + 1;
                op3 = Math.ceil(Math.random() * 5) + 1;
                op4 = Math.ceil(Math.random() * 5) + 1;
                op5 = Math.ceil(Math.random() * 5) + 1;
                op6 = Math.ceil(Math.random() * 5) + 1;



                return {
                    ques: `Find the product. Simplify your answer..</br>
       						$ -4f(-3f^{2}-6) $`,
                    Hint: ``,
                    Answer: ` $  12f^{3}+24f  $  `
                };

                // This return statement is responsible for displaying only question replace it same as 1st one
                return
            },
            answer: function() {
                return op1 * op2;
            }
        },



        {
            op1: 0,
            op2: 0,
            op3: 0,
            op4: 0,
            question: function() {
                op1 = Math.ceil(Math.random() * 5) + 1;
                op2 = Math.ceil(Math.random() * 5) + 1;
                op3 = Math.ceil(Math.random() * 5) + 1;
                op4 = Math.ceil(Math.random() * 5) + 1;
                op5 = Math.ceil(Math.random() * 5) + 1;
                op6 = Math.ceil(Math.random() * 5) + 1;



                return {
                    ques: `Write a polynomial of least degree with roots 3 and 4.</br>
       Write your answer using the variable x and in standard form with a leading coefficient of 1.`,
                    Hint: ``,
                    Answer: ` $  x^{2}-7x+12  $  `
                };

                // This return statement is responsible for displaying only question replace it same as 1st one
                return
            },
            answer: function() {
                return op1 * op2;
            }
        },



        {
            op1: 0,
            op2: 0,
            op3: 0,
            op4: 0,
            question: function() {
                op1 = Math.ceil(Math.random() * 5) + 1;
                op2 = Math.ceil(Math.random() * 5) + 1;
                op3 = Math.ceil(Math.random() * 5) + 1;
                op4 = Math.ceil(Math.random() * 5) + 1;
                op5 = Math.ceil(Math.random() * 5) + 1;
                op6 = Math.ceil(Math.random() * 5) + 1;



                return {
                    ques: `Match each polynomial function to its graph.</br>
       				$ f(x) = - x^{2} - 6 	  g(x) =2x^{2} + 16x +32 $	`,
                    Hint: ``,
                    Answer: ` In first graph $ g(x) =2x^{2} + 16x +32 $ and in second graph $  f(x) = - x^{2} - 6 $  `
                };

                // This return statement is responsible for displaying only question replace it same as 1st one
                return
            },
            answer: function() {
                return op1 * op2;
            }
        },



        {
            op1: 0,
            op2: 0,
            op3: 0,
            op4: 0,
            question: function() {
                op1 = Math.ceil(Math.random() * 5) + 1;
                op2 = Math.ceil(Math.random() * 5) + 1;
                op3 = Math.ceil(Math.random() * 5) + 1;
                op4 = Math.ceil(Math.random() * 5) + 1;
                op5 = Math.ceil(Math.random() * 5) + 1;
                op6 = Math.ceil(Math.random() * 5) + 1;



                return {
                    ques: `Solve using elimination.</br>
       					 x + 6y = 17
       					 2x + 3y = -2 `,
                    Hint: ``,
                    Answer: ` -7, 4 `
                };

                // This return statement is responsible for displaying only question replace it same as 1st one
                return
            },
            answer: function() {
                return op1 * op2;
            }
        },



        {
            op1: 0,
            op2: 0,
            op3: 0,
            op4: 0,
            question: function() {
                op1 = Math.ceil(Math.random() * 5) + 1;
                op2 = Math.ceil(Math.random() * 5) + 1;
                op3 = Math.ceil(Math.random() * 5) + 1;
                op4 = Math.ceil(Math.random() * 5) + 1;
                op5 = Math.ceil(Math.random() * 5) + 1;
                op6 = Math.ceil(Math.random() * 5) + 1;



                return {
                    ques: `How many solutions does this equation have?</br>
      						 -9m - 9 = - 9 - 9m 				`,
                    Hint: ``,
                    Answer: ` Infinitely many solutions `
                };

                // This return statement is responsible for displaying only question replace it same as 1st one
                return
            },
            answer: function() {
                return op1 * op2;
            }
        },



        {
            op1: 0,
            op2: 0,
            op3: 0,
            op4: 0,
            question: function() {
                op1 = Math.ceil(Math.random() * 5) + 1;
                op2 = Math.ceil(Math.random() * 5) + 1;
                op3 = Math.ceil(Math.random() * 5) + 1;
                op4 = Math.ceil(Math.random() * 5) + 1;
                op5 = Math.ceil(Math.random() * 5) + 1;
                op6 = Math.ceil(Math.random() * 5) + 1;



                return {
                    ques: ` How many solutions does the system of equations below have?</br>
      					 	y = 7x + 3 
    					   $ y= (1/9)x - 5/6 $`,
                    Hint: ``,
                    Answer: ` One solution `
                };

                // This return statement is responsible for displaying only question replace it same as 1st one
                return
            },
            answer: function() {
                return op1 * op2;
            }
        },


        {
            op1: 0,
            op2: 0,
            op3: 0,
            op4: 0,
            question: function() {
                op1 = Math.ceil(Math.random() * 5) + 1;
                op2 = Math.ceil(Math.random() * 5) + 1;
                op3 = Math.ceil(Math.random() * 5) + 1;
                op4 = Math.ceil(Math.random() * 5) + 1;
                op5 = Math.ceil(Math.random() * 5) + 1;
                op6 = Math.ceil(Math.random() * 5) + 1;



                return {
                    ques: `	 Write a system of equations to describe the situation below, solve using any method, and fill in the blanks.</br>
              
       							In order to burn calories and lose weight, Kinsley is trying to incorporate more exercise into her busy schedule.

        						She has several short exercise DVDs she can complete at home.</br>
       						 Last week, she burned a total of 665 calories by doing 5 body sculpting workouts and 5 yoga sessions.
        					 This week, she has completed 5 body sculpting workouts and 2 yoga sessions and burned a total of 434 calories.
        					How many calories does each workout burn?	`,
                    Hint: ``,
                    Answer: ` A body sculpting workout burns 56 calories and a yoga workout burns 77 calories. `
                };

                // This return statement is responsible for displaying only question replace it same as 1st one
                return
            },
            answer: function() {
                return op1 * op2;
            }
        },

        {
            op1: 0,
            op2: 0,
            op3: 0,
            op4: 0,
            question: function() {
                op1 = Math.ceil(Math.random() * 5) + 1;
                op2 = Math.ceil(Math.random() * 5) + 1;
                op3 = Math.ceil(Math.random() * 5) + 1;
                op4 = Math.ceil(Math.random() * 5) + 1;
                op5 = Math.ceil(Math.random() * 5) + 1;
                op6 = Math.ceil(Math.random() * 5) + 1;



                return {
                    ques: `	Solve the system of equations.</br>
       					$ y=x^{2}+17x-20 $
       						$ y=30x+28 $
       Write the coordinates in exact form. Simplify all fractions and radicals.	`,
                    Hint: ``,
                    Answer: ` (16, 508 ) (-3, -62) `
                };

                // This return statement is responsible for displaying only question replace it same as 1st one
                return
            },
            answer: function() {
                return op1 * op2;
            }
        },





        {
            op1: 0,
            op2: 0,
            op3: 0,
            op4: 0,
            question: function() {
                op1 = Math.ceil(Math.random() * 5) + 1;
                op2 = Math.ceil(Math.random() * 5) + 1;
                op3 = Math.ceil(Math.random() * 5) + 1;
                op4 = Math.ceil(Math.random() * 5) + 1;
                op5 = Math.ceil(Math.random() * 5) + 1;
                op6 = Math.ceil(Math.random() * 5) + 1;



                return {
                    ques: `	Look at this diagram
                    `,
                    Hint: ``,
                    Answer: ` 136 `
                };

                // This return statement is responsible for displaying only question replace it same as 1st one
                return
            },
            answer: function() {
                return op1 * op2;
            }
        },




        {
            op1: 0,
            op2: 0,
            op3: 0,
            op4: 0,
            question: function() {
                op1 = Math.ceil(Math.random() * 5) + 1;
                op2 = Math.ceil(Math.random() * 5) + 1;
                op3 = Math.ceil(Math.random() * 5) + 1;
                op4 = Math.ceil(Math.random() * 5) + 1;
                op5 = Math.ceil(Math.random() * 5) + 1;
                op6 = Math.ceil(Math.random() * 5) + 1;



                return {
                    ques: `	What is  m<1 		`,
                    Hint: ``,
                    Answer: ` 68 `
                };

                // This return statement is responsible for displaying only question replace it same as 1st one
                return
            },
            answer: function() {
                return op1 * op2;
            }
        },




        {
            op1: 0,
            op2: 0,
            op3: 0,
            op4: 0,
            question: function() {
                op1 = Math.ceil(Math.random() * 5) + 1;
                op2 = Math.ceil(Math.random() * 5) + 1;
                op3 = Math.ceil(Math.random() * 5) + 1;
                op4 = Math.ceil(Math.random() * 5) + 1;
                op5 = Math.ceil(Math.random() * 5) + 1;
                op6 = Math.ceil(Math.random() * 5) + 1;



                return {
                    ques: `	What is the value of t?	`,
                    Hint: ``,
                    Answer: ` 70 `
                };

                // This return statement is responsible for displaying only question replace it same as 1st one
                return
            },
            answer: function() {
                return op1 * op2;
            }
        },



        {
            op1: 0,
            op2: 0,
            op3: 0,
            op4: 0,
            question: function() {
                op1 = Math.ceil(Math.random() * 5) + 1;
                op2 = Math.ceil(Math.random() * 5) + 1;
                op3 = Math.ceil(Math.random() * 5) + 1;
                op4 = Math.ceil(Math.random() * 5) + 1;
                op5 = Math.ceil(Math.random() * 5) + 1;
                op6 = Math.ceil(Math.random() * 5) + 1;



                return {
                    ques: `	The radius of a circle is 1. What is the length of an arc that subtends an angle of</br>
                    			 πi/5   radians?	`,
                    Hint: ``,
                    Answer: ` π/5 `
                };

                // This return statement is responsible for displaying only question replace it same as 1st one
                return
            },
            answer: function() {
                return op1 * op2;
            }
        },


        {
            op1: 0,
            op2: 0,
            op3: 0,
            op4: 0,
            question: function() {
                op1 = Math.ceil(Math.random() * 5) + 1;
                op2 = Math.ceil(Math.random() * 5) + 1;
                op3 = Math.ceil(Math.random() * 5) + 1;
                op4 = Math.ceil(Math.random() * 5) + 1;
                op5 = Math.ceil(Math.random() * 5) + 1;
                op6 = Math.ceil(Math.random() * 5) + 1;



                return {
                    ques: `	 The diagram shows a triangle.
                    </br> What is the value of w?	`,
                    Hint: ``,
                    Answer: ` 37 `
                };

                // This return statement is responsible for displaying only question replace it same as 1st one
                return
            },
            answer: function() {
                return op1 * op2;
            }
        },


        {
            op1: 0,
            op2: 0,
            op3: 0,
            op4: 0,
            question: function() {
                op1 = Math.ceil(Math.random() * 5) + 1;
                op2 = Math.ceil(Math.random() * 5) + 1;
                op3 = Math.ceil(Math.random() * 5) + 1;
                op4 = Math.ceil(Math.random() * 5) + 1;
                op5 = Math.ceil(Math.random() * 5) + 1;
                op6 = Math.ceil(Math.random() * 5) + 1;



                return {
                    ques: `    ΔGHI ~ ΔUVW. Find HI.	`,
                    Hint: ``,
                    Answer: ` 8 `
                };

                // This return statement is responsible for displaying only question replace it same as 1st one
                return
            },
            answer: function() {
                return op1 * op2;
            }
        },



        {
            op1: 0,
            op2: 0,
            op3: 0,
            op4: 0,
            question: function() {
                op1 = Math.ceil(Math.random() * 5) + 1;
                op2 = Math.ceil(Math.random() * 5) + 1;
                op3 = Math.ceil(Math.random() * 5) + 1;
                op4 = Math.ceil(Math.random() * 5) + 1;
                op5 = Math.ceil(Math.random() * 5) + 1;
                op6 = Math.ceil(Math.random() * 5) + 1;



                return {
                    ques: `	Find m.</br>
                      Write your answer as a whole number or a decimal. Do not round.		`,
                    Hint: ``,
                    Answer: ` 4 `
                };

                // This return statement is responsible for displaying only question replace it same as 1st one
                return
            },
            answer: function() {
                return op1 * op2;
            }
        },


        {
            op1: 0,
            op2: 0,
            op3: 0,
            op4: 0,
            question: function() {
                op1 = Math.ceil(Math.random() * 5) + 1;
                op2 = Math.ceil(Math.random() * 5) + 1;
                op3 = Math.ceil(Math.random() * 5) + 1;
                op4 = Math.ceil(Math.random() * 5) + 1;
                op5 = Math.ceil(Math.random() * 5) + 1;
                op6 = Math.ceil(Math.random() * 5) + 1;



                return {
                    ques: `	What is the length of the hypotenuse?.  	`,
                    Hint: ``,
                    Answer: ` 40 `
                };

                // This return statement is responsible for displaying only question replace it same as 1st one
                return
            },
            answer: function() {
                return op1 * op2;
            }
        },


        {
            op1: 0,
            op2: 0,
            op3: 0,
            op4: 0,
            question: function() {
                op1 = Math.ceil(Math.random() * 5) + 1;
                op2 = Math.ceil(Math.random() * 5) + 1;
                op3 = Math.ceil(Math.random() * 5) + 1;
                op4 = Math.ceil(Math.random() * 5) + 1;
                op5 = Math.ceil(Math.random() * 5) + 1;
                op6 = Math.ceil(Math.random() * 5) + 1;



                return {
                    ques: `	Find n..</br>
                       Write your answer in simplest radical form.	`,
                    Hint: ``,
                    Answer: ` 8 `
                };

                // This return statement is responsible for displaying only question replace it same as 1st one
                return
            },
            answer: function() {
                return op1 * op2;
            }
        },



        {
            op1: 0,
            op2: 0,
            op3: 0,
            op4: 0,
            question: function() {
                op1 = Math.ceil(Math.random() * 5) + 1;
                op2 = Math.ceil(Math.random() * 5) + 1;
                op3 = Math.ceil(Math.random() * 5) + 1;
                op4 = Math.ceil(Math.random() * 5) + 1;
                op5 = Math.ceil(Math.random() * 5) + 1;
                op6 = Math.ceil(Math.random() * 5) + 1;



                return {
                    ques: `		Find TU	`,
                    Hint: ``,
                    Answer: ` 17 `
                };

                // This return statement is responsible for displaying only question replace it same as 1st one
                return
            },
            answer: function() {
                return op1 * op2;
            }
        },



        {
            op1: 0,
            op2: 0,
            op3: 0,
            op4: 0,
            question: function() {
                op1 = Math.ceil(Math.random() * 5) + 1;
                op2 = Math.ceil(Math.random() * 5) + 1;
                op3 = Math.ceil(Math.random() * 5) + 1;
                op4 = Math.ceil(Math.random() * 5) + 1;
                op5 = Math.ceil(Math.random() * 5) + 1;
                op6 = Math.ceil(Math.random() * 5) + 1;



                return {
                    ques: `This graph shows how the number of spools of thread that Elise has left is
                     related to the number of shirts that she sews. </br>
                         If Elise sews 2 shirts, how many spools of thread will she have left?\\  	`,
                    Hint: ``,
                    Answer: ` 4 spools of thread `
                };

                // This return statement is responsible for displaying only question replace it same as 1st one
                return
            },
            answer: function() {
                return op1 * op2;
            }
        },



        {
            op1: 0,
            op2: 0,
            op3: 0,
            op4: 0,
            question: function() {
                op1 = Math.ceil(Math.random() * 5) + 1;
                op2 = Math.ceil(Math.random() * 5) + 1;
                op3 = Math.ceil(Math.random() * 5) + 1;
                op4 = Math.ceil(Math.random() * 5) + 1;
                op5 = Math.ceil(Math.random() * 5) + 1;
                op6 = Math.ceil(Math.random() * 5) + 1;



                return {
                    ques: `	Rewrite the following equation in slope-intercept form..</br>
       				 y - 1 = 4(x - 1)
      					 Write your answer using integers, proper fractions, and improper fractions in simplest form.
       \\	`,
                    Hint: ``,
                    Answer: ` y=4x-3 `
                };

                // This return statement is responsible for displaying only question replace it same as 1st one
                return
            },
            answer: function() {
                return op1 * op2;
            }
        },



        {
            op1: 0,
            op2: 0,
            op3: 0,
            op4: 0,
            question: function() {
                op1 = Math.ceil(Math.random() * 5) + 1;
                op2 = Math.ceil(Math.random() * 5) + 1;
                op3 = Math.ceil(Math.random() * 5) + 1;
                op4 = Math.ceil(Math.random() * 5) + 1;
                op5 = Math.ceil(Math.random() * 5) + 1;
                op6 = Math.ceil(Math.random() * 5) + 1;



                return {
                    ques: `	What is TU		`,
                    Hint: ``,
                    Answer: ` 110 `
                };

                // This return statement is responsible for displaying only question replace it same as 1st one
                return
            },
            answer: function() {
                return op1 * op2;
            }
        },





        {
            op1: 0,
            op2: 0,
            op3: 0,
            op4: 0,
            question: function() {
                op1 = Math.ceil(Math.random() * 5) + 1;
                op2 = Math.ceil(Math.random() * 5) + 1;
                op3 = Math.ceil(Math.random() * 5) + 1;
                op4 = Math.ceil(Math.random() * 5) + 1;
                op5 = Math.ceil(Math.random() * 5) + 1;
                op6 = Math.ceil(Math.random() * 5) + 1;



                return {
                    ques: `	The radius of a circle is 6 miles. What is the length of a 135° arc?       	`,
                    Hint: ``,
                    Answer: ` 9/2π `
                };

                // This return statement is responsible for displaying only question replace it same as 1st one
                return
            },
            answer: function() {
                return op1 * op2;
            }
        },






        {
            op1: 0,
            op2: 0,
            op3: 0,
            op4: 0,
            question: function() {
                op1 = Math.ceil(Math.random() * 5) + 1;
                op2 = Math.ceil(Math.random() * 5) + 1;
                op3 = Math.ceil(Math.random() * 5) + 1;
                op4 = Math.ceil(Math.random() * 5) + 1;
                op5 = Math.ceil(Math.random() * 5) + 1;
                op6 = Math.ceil(Math.random() * 5) + 1;



                return {
                    ques: `	What is UW? 		`,
                    Hint: ``,
                    Answer: ` 20`
                };

                // This return statement is responsible for displaying only question replace it same as 1st one
                return
            },
            answer: function() {
                return op1 * op2;
            }
        },






        {
            op1: 0,
            op2: 0,
            op3: 0,
            op4: 0,
            question: function() {
                op1 = Math.ceil(Math.random() * 5) + 1;
                op2 = Math.ceil(Math.random() * 5) + 1;
                op3 = Math.ceil(Math.random() * 5) + 1;
                op4 = Math.ceil(Math.random() * 5) + 1;
                op5 = Math.ceil(Math.random() * 5) + 1;
                op6 = Math.ceil(Math.random() * 5) + 1;



                return {
                    ques: `	 The radius of a circle is 3 miles. What is the area of a sector bounded by a 180° arc?</br>
                      Give the exact answer in simplest form.  		`,
                    Hint: ``,
                    Answer: ` (9/2) π  `
                };

                // This return statement is responsible for displaying only question replace it same as 1st one
                return
            },
            answer: function() {
                return op1 * op2;
            }
        },






        {
            op1: 0,
            op2: 0,
            op3: 0,
            op4: 0,
            question: function() {
                op1 = Math.ceil(Math.random() * 5) + 1;
                op2 = Math.ceil(Math.random() * 5) + 1;
                op3 = Math.ceil(Math.random() * 5) + 1;
                op4 = Math.ceil(Math.random() * 5) + 1;
                op5 = Math.ceil(Math.random() * 5) + 1;
                op6 = Math.ceil(Math.random() * 5) + 1;



                return {
                    ques: `	 Find the tangent of ∠K.		`,
                    Hint: ``,
                    Answer: ` 56/33 `
                };

                // This return statement is responsible for displaying only question replace it same as 1st one
                return
            },
            answer: function() {
                return op1 * op2;
            }
        },






        {
            op1: 0,
            op2: 0,
            op3: 0,
            op4: 0,
            question: function() {
                op1 = Math.ceil(Math.random() * 5) + 1;
                op2 = Math.ceil(Math.random() * 5) + 1;
                op3 = Math.ceil(Math.random() * 5) + 1;
                op4 = Math.ceil(Math.random() * 5) + 1;
                op5 = Math.ceil(Math.random() * 5) + 1;
                op6 = Math.ceil(Math.random() * 5) + 1;



                return {
                    ques: `	Find m∠C	`,
                    Hint: ``,
                    Answer: ` 30 `
                };

                // This return statement is responsible for displaying only question replace it same as 1st one
                return
            },
            answer: function() {
                return op1 * op2;
            }
        },






        {
            op1: 0,
            op2: 0,
            op3: 0,
            op4: 0,
            question: function() {
                op1 = Math.ceil(Math.random() * 5) + 1;
                op2 = Math.ceil(Math.random() * 5) + 1;
                op3 = Math.ceil(Math.random() * 5) + 1;
                op4 = Math.ceil(Math.random() * 5) + 1;
                op5 = Math.ceil(Math.random() * 5) + 1;
                op6 = Math.ceil(Math.random() * 5) + 1;



                return {
                    ques: `cos(x) = -0.1  What is sin(90°-x)? 		`,
                    Hint: ``,
                    Answer: ` -0.1 `
                };

                // This return statement is responsible for displaying only question replace it same as 1st one
                return
            },
            answer: function() {
                return op1 * op2;
            }
        },






        {
            op1: 0,
            op2: 0,
            op3: 0,
            op4: 0,
            question: function() {
                op1 = Math.ceil(Math.random() * 5) + 1;
                op2 = Math.ceil(Math.random() * 5) + 1;
                op3 = Math.ceil(Math.random() * 5) + 1;
                op4 = Math.ceil(Math.random() * 5) + 1;
                op5 = Math.ceil(Math.random() * 5) + 1;
                op6 = Math.ceil(Math.random() * 5) + 1;



                return {
                    ques: `	What is the area of the shaded region? </br>

                    Write your answer as a whole number or a decimal rounded to the nearest hundredth.	`,
                    Hint: ``,
                    Answer: ` 487 `
                };

                // This return statement is responsible for displaying only question replace it same as 1st one
                return
            },
            answer: function() {
                return op1 * op2;
            }
        },






        {
            op1: 0,
            op2: 0,
            op3: 0,
            op4: 0,
            question: function() {
                op1 = Math.ceil(Math.random() * 5) + 1;
                op2 = Math.ceil(Math.random() * 5) + 1;
                op3 = Math.ceil(Math.random() * 5) + 1;
                op4 = Math.ceil(Math.random() * 5) + 1;
                op5 = Math.ceil(Math.random() * 5) + 1;
                op6 = Math.ceil(Math.random() * 5) + 1;



                return {
                    ques: `	What is the volume?.	`,
                    Hint: ``,
                    Answer: ` 729 `
                };

                // This return statement is responsible for displaying only question replace it same as 1st one
                return
            },
            answer: function() {
                return op1 * op2;
            }
        },






        {
            op1: 0,
            op2: 0,
            op3: 0,
            op4: 0,
            question: function() {
                op1 = Math.ceil(Math.random() * 5) + 1;
                op2 = Math.ceil(Math.random() * 5) + 1;
                op3 = Math.ceil(Math.random() * 5) + 1;
                op4 = Math.ceil(Math.random() * 5) + 1;
                op5 = Math.ceil(Math.random() * 5) + 1;
                op6 = Math.ceil(Math.random() * 5) + 1;



                return {
                    ques: `	What is the area of this figure?		`,
                    Hint: ``,
                    Answer: ` 84 `
                };

                // This return statement is responsible for displaying only question replace it same as 1st one
                return
            },
            answer: function() {
                return op1 * op2;
            }
        },






        {
            op1: 0,
            op2: 0,
            op3: 0,
            op4: 0,
            question: function() {
                op1 = Math.ceil(Math.random() * 5) + 1;
                op2 = Math.ceil(Math.random() * 5) + 1;
                op3 = Math.ceil(Math.random() * 5) + 1;
                op4 = Math.ceil(Math.random() * 5) + 1;
                op5 = Math.ceil(Math.random() * 5) + 1;
                op6 = Math.ceil(Math.random() * 5) + 1;



                return {
                    ques: ` Lillian has the following data:?</br>
       							{n 58 66 61 59 60 61} 
      						 If the median is 60, which number could n be?	`,
                    Hint: ``,
                    Answer: ` 58 `
                };

                // This return statement is responsible for displaying only question replace it same as 1st one
                return
            },
            answer: function() {
                return op1 * op2;
            }
        },






        {
            op1: 0,
            op2: 0,
            op3: 0,
            op4: 0,
            question: function() {
                op1 = Math.ceil(Math.random() * 5) + 1;
                op2 = Math.ceil(Math.random() * 5) + 1;
                op3 = Math.ceil(Math.random() * 5) + 1;
                op4 = Math.ceil(Math.random() * 5) + 1;
                op5 = Math.ceil(Math.random() * 5) + 1;
                op6 = Math.ceil(Math.random() * 5) + 1;



                return {
                    ques: `	Does this scatter plot show a positive trend, a negative trend, or no trend?	`,
                    Hint: ``,
                    Answer: ` Positive Trend `
                };

                // This return statement is responsible for displaying only question replace it same as 1st one
                return
            },
            answer: function() {
                return op1 * op2;
            }
        },






        {
            op1: 0,
            op2: 0,
            op3: 0,
            op4: 0,
            question: function() {
                op1 = Math.ceil(Math.random() * 5) + 1;
                op2 = Math.ceil(Math.random() * 5) + 1;
                op3 = Math.ceil(Math.random() * 5) + 1;
                op4 = Math.ceil(Math.random() * 5) + 1;
                op5 = Math.ceil(Math.random() * 5) + 1;
                op6 = Math.ceil(Math.random() * 5) + 1;



                return {
                    ques: `	What is the equation of the trend line in the scatter plot?</br>


                       What is the equation of the trend line in the scatter plot?
      Use the two yellow points to write the equation in slope-intercept form.</br>
       Write any coefficients as integers, proper fractions, or improper fractions in simplest form.\\	`,
                    Hint: ``,
                    Answer: ` $ y=(5/6)x +4  $ `
                };

                // This return statement is responsible for displaying only question replace it same as 1st one
                return
            },
            answer: function() {
                return op1 * op2;
            }
        },






        {
            op1: 0,
            op2: 0,
            op3: 0,
            op4: 0,
            question: function() {
                op1 = Math.ceil(Math.random() * 5) + 1;
                op2 = Math.ceil(Math.random() * 5) + 1;
                op3 = Math.ceil(Math.random() * 5) + 1;
                op4 = Math.ceil(Math.random() * 5) + 1;
                op5 = Math.ceil(Math.random() * 5) + 1;
                op6 = Math.ceil(Math.random() * 5) + 1;



                return {
                    ques: `Ashley surveyed the 37 book critics who read and reviewed a new novel.</br>
       					 Is this sample of the readers of the new novel likely to be biased??	`,
                    Hint: ``,
                    Answer: ` yes `
                };

                // This return statement is responsible for displaying only question replace it same as 1st one
                return
            },
            answer: function() {
                return op1 * op2;
            }
        },






        {
            op1: 0,
            op2: 0,
            op3: 0,
            op4: 0,
            question: function() {
                op1 = Math.ceil(Math.random() * 5) + 1;
                op2 = Math.ceil(Math.random() * 5) + 1;
                op3 = Math.ceil(Math.random() * 5) + 1;
                op4 = Math.ceil(Math.random() * 5) + 1;
                op5 = Math.ceil(Math.random() * 5) + 1;
                op6 = Math.ceil(Math.random() * 5) + 1;



                return {
                    ques: `	A wilderness retail store asked a consulting company to do an analysis of their hiking shoe customers.</br>
                     The consulting company gathered data 
                    from each customer that purchased hiking shoes, and recorded the shoe brand and the customer's level of happiness.	</br>



                          What is the probability that a randomly selected customer purchased a Footlong shoe and is pleased?	`,
                    Hint: ``,
                    Answer: ` 3/10 `
                };

                // This return statement is responsible for displaying only question replace it same as 1st one
                return
            },
            answer: function() {
                return op1 * op2;
            }
        },







    ]

};

$(document).ready(function() {
    var questionField = $("#question");

    // var totalQuestion = quiz.data.length;
    var totalQuestion = quiz.question;
    var currentIndex = 0;
    var progressField = $("#progress");
    var timerField = $("#timer");
    var intervalHandle = null;
    var hint = $("#hint");
    var answer = $(".answer");
    var image = $("#imageScript");



    // shuffle quiz.data
    if (quiz.randomize) {
        for (var i = quiz.data.length - 1; i > 0; --i) {
            var rand = Math.floor(Math.random() * i);
            var temp = quiz.data[i];
            quiz.data[i] = quiz.data[rand];
            quiz.data[rand] = temp;
        }
    }

    function loadQuestion() {
        progressField.text("Question " + (currentIndex + 1) + " of " + totalQuestion);

        var remainingSec = quiz.duration;

        var min = Math.floor(remainingSec / 60);
        var sec = remainingSec % 60;
        if (min < 10)
            min = "0" + min;
        if (sec < 10)
            sec = "0" + sec;
        timerField.text(min + ":" + sec);


        dataSet = quiz.data[currentIndex].question();

        questionField.html(dataSet.ques);
        hint.html(dataSet.Hint);
        answer.html(dataSet.Answer);
        image.html(dataSet.imageScript);

        MathJax.Hub.Queue(["Typeset", MathJax.Hub, questionField[0]]);
        MathJax.Hub.Queue(["Typeset", MathJax.Hub, hint[0]]);
        MathJax.Hub.Queue(["Typeset", MathJax.Hub, answer[0]]);


        setTimeout(function() {
            if (currentIndex === 0) {
                questionField.css("opacity", 1);
            }
            animateQuestion(remainingSec);
            $('.next_question').click(function() {

            })
        }, 500);
    }


    function animateQuestion(remainingSec) {
        questionField.removeClass("zoomOutLeft");
        questionField.addClass("zoomIn");
        setTimeout(function() {
            intervalHandle = setInterval(function() {
                remainingSec -= 1;
                if (remainingSec > 0) {
                    var min = Math.floor(remainingSec / 60);
                    var sec = remainingSec % 60;
                    if (min < 10)
                        min = "0" + min;
                    if (sec < 10)
                        sec = "0" + sec;
                    timerField.text(min + ":" + sec);
                    loadhint(sec);
                    loadanswer(sec);
                } else {
                    // unload the question

                    timerField.text("00:00");
                    clearInterval(intervalHandle);

                    questionField.removeClass("zoomIn");
                    questionField.addClass("zoomOutLeft");
                    setTimeout(function() {
                        if (currentIndex < totalQuestion - 1) {
                            loadQuestion(++currentIndex);
                        } else {
                            questionField.hide();
                            progressField.css("visibility", "hidden");
                            timerField.css("visibility", "hidden");
                            $("#resultBox").fadeIn();
                        }
                    }, 1000);
                }
            }, 1000);
        }, 1000);
    }

    function loadhint(second) {
        if (second <= 30 && second >= 5) {
            hint.fadeIn(2000);
        } else if (second >= 30) {
            $('.ask_hint').click(function() {
                hint.fadeIn(1000);
            });
        } else {
            hint.fadeOut(2000);
        }
    }

    function loadanswer(secondCountAns) {
        if (secondCountAns <= 15 && secondCountAns >= 5) {
            answer.fadeIn(2000);
        } else if (secondCountAns >= 15) {
            $('.check_answer').click(function() {
                answer.fadeIn(1000);
            });
        } else {
            answer.fadeOut(2000);
        }
    }

    loadQuestion(currentIndex);

    $('.choose').click(function() {
        $('.audio_container').slideToggle();
    });

    $('.audio_one').click(function() {
        $('#audioChange').attr('src', 'https://bluerocketacademy.com/wp-content/uploads/2020/09/Binaural.mp3');
        $('.audio_container').slideUp();
    })
    $('.audio_two').click(function() {
        $('#audioChange').attr('src', 'https://bluerocketacademy.com/wp-content/uploads/2020/09/Classical.mp3');
        $('.audio_container').slideUp();
    })
});