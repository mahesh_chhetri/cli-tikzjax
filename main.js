\
documentclass { article }\
usepackage[utf8] { inputenc }\
usepackage { amsmath } %
\usepackage { pgfpages }

%
\setlength\ columnseprule { 1 pt } %
\pgfpagesuselayout { 4 on 1 }[article]\ usepackage[dvipsnames] { xcolor } %
    \usepackage { multicol }\
usepackage { color }\
usepackage { fancyhdr, color }\
pagestyle { fancy }\
fancyhf {}\
setlength\ headheight { 9 pt }\
lhead {\
    textcolor { gray } {\ rule[-2 pt] {\ textwidth } { 15 pt } } % \hspace {-\textwidth } % \textcolor { black } {\ textbf { Page\ thepage } }
}\
usepackage[draft] { graphicx }\
usepackage { tabularx, colortbl }

\
usepackage { tikz }

\
setlength {\ tabcolsep } { 18 pt }\
renewcommand {\ arraystretch } { 2.5 }


%
\setlength {\ columnseprule } { 1 pt } %
\def\ columnseprulecolor {\ color { blue } }

\
begin { document }\
large\ begin { center }\
textbf {\ Huge Questions }\\\
end { center }


\
textbf {\\ Question 1. }
40\ % of the tickets sold at a school carnival were early - admission tickets.If the school sold 80 tickets in all, how many early - admission tickets did it sell ? \\


    \textbf {\\ Question 2. }\ % 7 is what percent of\ $7 ?
    Write your answer using a percent sign(\ % ).For example, 0.5\ % , 12.7\ % , or 56\ % .\\


\textbf {\\ Question 3. }
A sports shop has 71 employees.100\ % of the employees work part - time.How many part - time employees does the sports shop have ? \\

    \textbf {\\ Question 5. }
Now that Hillsdale Auto Repair has a different owner, its mechanics make\ $48 .21 per hour.That is 0\ % more than they originally made.How much did the mechanics use to make per hour ? \\

    \textbf {\\ Question 6. }
Gary wants to buy a skateboard.The original price is\(\$86 .10\) What is the sale price ?

    \newpage

\ textbf {\\ Question 7. }
Is this
function linear, quadratic, or exponential ? \\\vspace { 5 mm }\
begin { center }\
begin { tabular } { | c | c | }\
hline\ rowcolor { gray }
x & y\\\ hline -
    4 & 46\\\ hline -
    3 & 37\\\ hline -
    2 & 28\\\ hline -
    1 & 19\\\ hline
0 & 10\\\ hline\ end { tabular }\
end { center }\
vspace { 15 mm }\
textbf {\\ Question 8. }
What type of
function does this graph show ? \begin { center }\
resizebox { .6\ textwidth } {! } {\ begin { tikzpicture }[scale = 0.45]\ footnotesize\ foreach\ x in {-10, ..., 10 }\
    draw[line width = 0.15 pt](\x, -10) to(\x, 10);

    \
    foreach\ y in {-10, ..., 10 }\
    draw[line width = 0.15 pt](-10, \y) to(10, \y);

    \
    draw[{ latex } - { latex }, line width = 1.4 pt](0, -11) to(0, 11);\
    draw[{ latex } - { latex }, line width = 1.4 pt](-11, 0) to(11, 0);

    \
    draw[{ latex } - { latex }, line width = 1.8 pt, color = Gray](-10, -9) to(10, 5);\
    end { tikzpicture } }\
end { center }

\
textbf {\\ Question 9. }
Multiply.Write your answer in simplest form.\\\(-\sqrt { 2 }(-6 + \sqrt { 3 })\)\\

\ textbf {\\ Question 10. }
Simplify.\\\[8 ^ {\ frac { 1 } { 3 } }\]

\ textbf {\\ Question 11. }
Franklin takes 180 milligrams of an antibiotic.Every hour, his body breaks down 25\ % of the drug.How much will be left after 8 hours ? \\
    If necessary, round your answer to the nearest tenth.\\

\textbf {\\ Question 12. }
Simplify the expression.Assume all variables are positive\\\((64 s) ^ {\ frac { 1 } { 2 } }\)\\
Write your answer in the form A or\(\frac { A } { B }\), where A and B are constants or variable expressions that have no variables in common.All exponents in your answer should be positive

\ textbf {\\ Question 13. }
Look at this table: \\\begin { center }\
begin { tabular } { | c | c | }\
hline\ rowcolor { gray }
x & y\\\ hline -
    8 & 10\\\ hline -
    7 & 8\\\ hline -
    6 & 6\\\ hline -
    5 & 4\\\ hline -
    4 & 2\\\ hline\ end { tabular }\
end { center }
Write a linear(y = mx + b), quadratic\((y = ax ^ { 2 })\), or exponential\(y = a(b) ^ { x }\)
function that models the data.\\

\textbf {\\ Question 14. }
Solve
for j in the proportion.\\\[\frac { 24 } { 80 } = \frac { 3 } { j }\]\\

\ textbf {\\ Question 15. }
Brooke baked 10 cookies with 2 scoops of flour.How many scoops of flour does Brooke need in order to bake 15 cookies ? Assume the relationship is directly proportional.\\

\textbf {\\ Question 16. }
Layla loves to
try out new bike trails in her area.Last week, it took her 24 minutes to ride a trail that is 6 miles long.Today, she kept the same pace when she rode an 8 - mile trail.\\
How long did it take Layla to ride the trail today ? \\


    \textbf {\\ Question 17. }
Which equation gives the rule
for this table ? \\\begin { center }\
begin { tabular } { | c | c | }\
hline\ rowcolor { gray }
x & y\\\ hline
2 & 53\\\ hline
3 & 54\\\ hline
4 & 55\\\ hline
5 & 56\\\ hline\ end { tabular }\
end { center }\[y = x = 18\ quad y = x + 21\ quad y = x + 51\ quad y = x + 27\]\ newpage\ textbf {\\ Question 18. }
Nina jars 3 liters of jam everyday.How many days does Nina need to spend making jam
if she wants to jar 24 liters of jam in all ? \\

    \textbf {\\ Question 19. }
Solve
for u.\\\[\frac { u + 8 } { u + 10 } = \frac { u + 5 } { u + 6 }\]
There may be 1 or 2 solutions.\\

\textbf {\\ Question 20. }
Solve
for r in terms of C.\\\(C = 2\ pi r\)\\
r = ?

    \textbf {\\ Question 21. }
Solve
for u.\\\(-4 u + 9\ leq 1\)

\ textbf {\\ Question 22. }
Solve the system of inequalities by graphing.\\\(y < -1\)\\\(y > -7\)\\

Select a line to change it between solid and dotted.Select a region to shade it.\\\begin { center }\
resizebox { .6\ textwidth } {! } {\ begin { tikzpicture }[scale = 0.45]\ footnotesize\ foreach\ x in {-9, -7, -5, -3, -1, 1, 3, 5, 7, 9 }\
    draw[line width = 0.15 pt](\x, -10.5) to(\x, 10.5);\
    foreach\ x in {-10, -8, -6, -4, -2, 2, 4, 6, 8, 10 }\
    draw[line width = 0.15 pt](\x, -10.5) to(\x, 0) node[below] { $\ x$ }
    to(\x, 10.5);

    \
    foreach\ y in {-9, -7, -5, -3, -1, 1, 3, 5, 7, 9 }\
    draw[line width = 0.15 pt](-10.5, \y) to(10.5, \y);\
    foreach\ y in {-10, -8, -6, -4, -2, 2, 4, 6, 8, 10 }\
    draw[line width = 0.15 pt](-10.5, \y) to(0, \y) node[left] { $\ y$ }
    to(10.5, \y);


    \
    draw[fill = black, draw = black](1, 1) circle[radius = 4 pt];\
    node[fill = white, below left] at(0, 0) { $O$ };

    \
    draw[{ latex } - { latex }, line width = 1.4 pt](-10.9, 0) to(10.9, 0) node[right] { $x$ };\
    draw[{ latex } - { latex }, line width = 1.4 pt](0, -10.9) to(0, 10.9) node[above] { $y$ };\
    end { tikzpicture } }\
end { center }

\
textbf {\\ Question 23. }
The students at Oak Grove High School are selling candles and scented soaps to raise money
for a new computer lab.They will earn\ $2
for every candle they sell.Each bar of soap they sell will earn them\ $4.They need to raise a minimum of\ $2, 500 to have enough money to finish construction of the computer lab.
Select the inequality in standard form that describes this situation.Use the given numbers and the following variables.\\
x = the number of candles they sell\\
y = the number of soaps they sell\\

\[4 x + 2 y > 2500\]\[4 x + 2 y\ geq 2500\]\[2 x + 4 y > 2500\]\[2 x + 4 y\ geq 2500\]

\ textbf {\\ Question 24. }
Solve
for a.\\\(\sqrt { 4 - 2 a } = \sqrt { 2 a }\)\\
a = ? \\

    \textbf {\\ Question 25. }
Use the following
function rule to find f(4).\\\(f(x) = (4 + x) ^ { 2 }\)\\\(f(4) = \)\ newpage\ textbf {\\ Question 26. }
Match each quadratic
function to its graph.\\\(f(x) = -x ^ { 2 } - 10 x - 20\ qquad g(x) = x ^ { 2 } + 2 x - 6\)\\\ begin { center }\
resizebox { .9\ textwidth } {! } {\ begin { tikzpicture }[scale = 0.37]\ footnotesize\ begin { scope }\
    foreach\ x in {-9, -7, -5, -3, -1, 1, 3, 5, 7, 9 }\
    draw[line width = 0.15 pt](\x, -10.5) to(\x, 10.5);\
    foreach\ x in {-10, -8, -6, -4, -2, 2, 4, 6, 8, 10 }\
    draw[line width = 0.15 pt](\x, -10.5) to(\x, 0) node[below] { $\ x$ }
    to(\x, 10.5);

    \
    foreach\ y in {-9, -7, -5, -3, -1, 1, 3, 5, 7, 9 }\
    draw[line width = 0.15 pt](-10.5, \y) to(10.5, \y);\
    foreach\ y in {-10, -8, -6, -4, -2, 2, 4, 6, 8, 10 }\
    draw[line width = 0.15 pt](-10.5, \y) to(0, \y) node[left] { $\ y$ }
    to(10.5, \y);

    \
    node[fill = white, below left] at(0, 0) { $O$ };

    \
    draw[{ latex } - { latex }, line width = 1.4 pt](-10.9, 0) to(10.9, 0) node[above right] { $x$ };\
    draw[{ latex } - { latex }, line width = 1.4 pt](0, -10.9) to(0, 10.9) node[above] { $y$ };\
    draw[{ latex } - { latex }, line width = 1.5 pt, color = Black](-9, -10) parabola bend(-5, 5)(-1, -10);\
    end { scope }\
    begin { scope }[xshift = 24 cm]\ foreach\ x in {-9, -7, -5, -3, -1, 1, 3, 5, 7, 9 }\
    draw[line width = 0.15 pt](\x, -10.5) to(\x, 10.5);\
    foreach\ x in {-10, -8, -6, -4, -2, 2, 4, 6, 8, 10 }\
    draw[line width = 0.15 pt](\x, -10.5) to(\x, 0) node[below] { $\ x$ }
    to(\x, 10.5);

    \
    foreach\ y in {-9, -7, -5, -3, -1, 1, 3, 5, 7, 9 }\
    draw[line width = 0.15 pt](-10.5, \y) to(10.5, \y);\
    foreach\ y in {-10, -8, -6, -4, -2, 2, 4, 6, 8, 10 }\
    draw[line width = 0.15 pt](-10.5, \y) to(0, \y) node[left] { $\ y$ }
    to(10.5, \y);

    \
    node[fill = white, below left] at(0, 0) { $O$ };

    \
    draw[{ latex } - { latex }, line width = 1.4 pt](-10.9, 0) to(10.9, 0) node[above right] { $x$ };\
    draw[{ latex } - { latex }, line width = 1.4 pt](0, -10.9) to(0, 10.9) node[above] { $y$ };\
    draw[{ latex } - { latex }, line width = 1.5 pt, color = Black](-5, 10) parabola bend(-1, -7)(3, 10);\
    end { scope }\
    end { tikzpicture } }\
end { center }

\
textbf {\\ Question 27. }
Look at this graph: \\

    \begin { center }\
resizebox { .6\ textwidth } {! } {\ begin { tikzpicture }[scale = 0.45]\ footnotesize\ foreach\ x in {-10, ..., 10 }\
    draw[line width = 0.15 pt](\x, -10) to(\x, 10);

    \
    foreach\ y in {-10, ..., 10 }\
    draw[line width = 0.15 pt](-10, \y) to(10, \y);

    \
    node[fill = white, below] at(-10, 0) { $ - 10 $ };\
    node[fill = white, below] at(-5, 0) { $ - 5 $ };\
    node[fill = white, below] at(5, 0) { $5$ };\
    node[fill = white, below] at(10, 0) { $10$ };

    \
    node[fill = white, left] at(0, -10) { $ - 10 $ };\
    node[fill = white, left] at(0, -5) { $ - 5 $ };\
    node[fill = white, left] at(0, 5) { $5$ };\
    node[fill = white, left] at(0, 10) { $10$ };

    \
    node[fill = white, below left] at(0, 0) { $O$ };

    \
    draw[{ latex } - { latex }, line width = 1.4 pt](0, -11) to(0, 11) node[above] { $y$ };\
    draw[{ latex } - { latex }, line width = 1.4 pt](-11, 0) to(11, 0) node[right] { $x$ };

    \
    draw[{ latex } - { latex }, line width = 1.8 pt, color = Black](-2, 10) parabola bend(2, -4)(6, 10);\
    end { tikzpicture } }\
end { center }
What are the coordinates of the vertex ? \\\newpage\ textbf {\\ Question 28. }
Graph the
function\(f(x) = 6 x ^ { 2 }\)\\
Plot the vertex.Then plot another point on the parabola.If you make a mistake, you can erase your parabola by selecting the second point and placing it on top of the first.\\\begin { center }\
resizebox { .6\ textwidth } {! } {\ begin { tikzpicture }[scale = 0.45]\ footnotesize\ foreach\ x in {-9, -7, -5, -3, -1, 1, 3, 5, 7, 9 }\
    draw[line width = 0.15 pt](\x, -10.5) to(\x, 10.5);\
    foreach\ x in {-10, -8, -6, -4, -2, 2, 4, 6, 8, 10 }\
    draw[line width = 0.15 pt](\x, -10.5) to(\x, 0) node[below] { $\ x$ }
    to(\x, 10.5);

    \
    foreach\ y in {-9, -7, -5, -3, -1, 1, 3, 5, 7, 9 }\
    draw[line width = 0.15 pt](-10.5, \y) to(10.5, \y);\
    foreach\ y in {-10, -8, -6, -4, -2, 2, 4, 6, 8, 10 }\
    draw[line width = 0.15 pt](-10.5, \y) to(0, \y) node[left] { $\ y$ }
    to(10.5, \y);


    \
    draw[fill = black, draw = black](3.5, 2) circle[radius = 4 pt];\
    node[fill = white, below left] at(0, 0) { $O$ };

    \
    draw[{ latex } - { latex }, line width = 1.4 pt](-10.9, 0) to(10.9, 0) node[right] { $x$ };\
    draw[{ latex } - { latex }, line width = 1.4 pt](0, -10.9) to(0, 10.9) node[above] { $y$ };\
    end { tikzpicture } }\
end { center }

\
textbf {\\ Question 29. }
Find the discriminant.\\\(8 z ^ { 2 } + 8 z + 2 = 0\)

\ textbf {\\ Question 30. }
How many real solutions does the equation have ? ? \\\(m ^ { 2 } + 62 = 62\)

\ textbf {\\ Question 31. }
Solve
for m.\\\(m(m - 3) = 0\)
Write your answers as integers or as proper or improper fractions in simplest form.
m = ? , m = ?

    \textbf {\\ Question 32. }
Solve
for v.\\\(v ^ { 2 } + 8 v + 12 = 0\)\\
Write each solution as an integer, proper fraction, or improper fraction in simplest form.If there are multiple solutions, separate them with commas.\\

\textbf {\\ Question 33. }
Factor.\\\(2 w ^ { 2 } + 9 w + 10\)

\ textbf {\\ Question 34. }
Find the product.Simplify your answer.\\\((a + 2)(2 a - 4)\)\\

\ textbf {\\ Question 35. }
Find the product.Simplify your answer.\\\((3 k - 1)(3 k + 1)\)\\

\ textbf {\\ Question 36. }
Find the product.Simplify your answer..\\\(-4 f(-3 f ^ { 2 } - 6)\)

\ textbf {\\ Question 37. }
Write a polynomial of least degree with roots 3 and 4.\\
Write your answer using the variable x and in standard form with a leading coefficient of 1.\\

\ textbf {\\ Question 38. }
Match each polynomial
function to its graph.\\\(f(x) = -x ^ { 2 } - 6\ qquad g(x) = 2 x ^ { 2 } + 16 x + 32\)\\\ begin { center }\
resizebox { .9\ textwidth } {! } {\ begin { tikzpicture }[scale = 0.37]\ footnotesize\ begin { scope }\
    foreach\ x in {-9, -7, -5, -3, -1, 1, 3, 5, 7, 9 }\
    draw[line width = 0.15 pt](\x, -10.5) to(\x, 10.5);\
    foreach\ x in {-10, -8, -6, -4, -2, 2, 4, 6, 8, 10 }\
    draw[line width = 0.15 pt](\x, -10.5) to(\x, 0) node[below] { $\ x$ }
    to(\x, 10.5);

    \
    foreach\ y in {-9, -7, -5, -3, -1, 1, 3, 5, 7, 9 }\
    draw[line width = 0.15 pt](-10.5, \y) to(10.5, \y);\
    foreach\ y in {-10, -8, -6, -4, -2, 2, 4, 6, 8, 10 }\
    draw[line width = 0.15 pt](-10.5, \y) to(0, \y) node[left] { $\ y$ }
    to(10.5, \y);

    \
    node[fill = white, below left] at(0, 0) { $O$ };

    \
    draw[{ latex } - { latex }, line width = 1.4 pt](-10.9, 0) to(10.9, 0) node[above right] { $x$ };\
    draw[{ latex } - { latex }, line width = 1.4 pt](0, -10.9) to(0, 10.9) node[above] { $y$ };\
    draw[{ latex } - { latex }, line width = 1.5 pt, color = black](-6.1, 10) parabola bend(-4, 0)(-1.9, 10);\
    end { scope }\
    begin { scope }[xshift = 24 cm]\ foreach\ x in {-9, -7, -5, -3, -1, 1, 3, 5, 7, 9 }\
    draw[line width = 0.15 pt](\x, -10.5) to(\x, 10.5);\
    foreach\ x in {-10, -8, -6, -4, -2, 2, 4, 6, 8, 10 }\
    draw[line width = 0.15 pt](\x, -10.5) to(\x, 0) node[below] { $\ x$ }
    to(\x, 10.5);

    \
    foreach\ y in {-9, -7, -5, -3, -1, 1, 3, 5, 7, 9 }\
    draw[line width = 0.15 pt](-10.5, \y) to(10.5, \y);\
    foreach\ y in {-10, -8, -6, -4, -2, 2, 4, 6, 8, 10 }\
    draw[line width = 0.15 pt](-10.5, \y) to(0, \y) node[left] { $\ y$ }
    to(10.5, \y);

    \
    node[fill = white, below left] at(0, 0) { $O$ };

    \
    draw[{ latex } - { latex }, line width = 1.4 pt](-10.9, 0) to(10.9, 0) node[above right] { $x$ };\
    draw[{ latex } - { latex }, line width = 1.4 pt](0, -10.9) to(0, 10.9) node[above] { $y$ };\
    draw[{ latex } - { latex }, line width = 1.5 pt, color = black](-2, -10) parabola bend(0, -6)(2, -10);\
    end { scope }\
    end { tikzpicture } }\
end { center }

\
textbf {\\ Question 39. }
Solve using elimination.\\\(x + 6 y = 17\)\(2 x + 3 y = -2\)

\ textbf {\\ Question 40. }
How many solutions does this equation have ? \\\(-9 m - 9 = -9 - 9 m\)

\ textbf {\\ Question 41. }
How many solutions does the system of equations below have ? \\\(y = 7 x + 3\)\\\(y = \frac { 1 } { 9 }
    x - \frac { 5 } { 6 }\)\\

\ textbf {\\ Question 42. }
Write a system of equations to describe the situation below, solve using any method, and fill in the blanks.\\

In order to burn calories and lose weight, Kinsley is trying to incorporate more exercise into her busy schedule.She has several short exercise DVDs she can complete at home.Last week, she burned a total of 665 calories by doing 5 body sculpting workouts and 5 yoga sessions.This week, she has completed 5 body sculpting workouts and 2 yoga sessions and burned a total of 434 calories.How many calories does each workout burn ? \\

    \textbf {\\ Question 43. }
Solve the system of equations.\\\(y = x ^ { 2 } + 17 x - 20\)\\\(y = 30 x + 28\)\\
Write the coordinates in exact form.Simplify all fractions and radicals.\\

\textbf {\\ Question 44. }
Look at this diagram: \\\begin { center }\
resizebox { .25\ textwidth } {! } {\ begin { tikzpicture }[scale = 0.5]\ small\ draw[{ latex } - { latex }, line width = 1 pt](-1, 0) to(7, 0);\
    draw[{ latex } - { latex }, line width = 1 pt](-1, 3) to(7, 3);\
    draw[{ latex } - { latex }, line width = 1 pt](0, -2) to(6, 5);

    \
    draw[fill = black, draw = black](0, 0) circle[radius = 4 pt];\
    draw[fill = black, draw = black](6, 0) circle[radius = 4 pt];\
    draw[fill = black, draw = black](0, 3) circle[radius = 4 pt];\
    draw[fill = black, draw = black](6, 3) circle[radius = 4 pt];

    \
    draw[fill = black, draw = black](0.6, -1.3) circle[radius = 4 pt];\
    draw[fill = black, draw = black](1.714, 0) circle[radius = 4 pt];\
    draw[fill = black, draw = black](4.286, 3) circle[radius = 4 pt];\
    draw[fill = black, draw = black](5.4, 4.3) circle[radius = 4 pt];\
    end { tikzpicture } }\
end { center }

If\(\overleftrightarrow { AB }\) and\(\overleftrightarrow { VX }\)
are parallel lines and\(m < XWY = 136 ^ {\ circ }\), what is\(m < UTW\) ? \newpage

\ textbf {\\ Question 45. }
What is\(m < 1\) ? \\\begin { center }\
resizebox { .2\ textwidth } {! } {\ begin { tikzpicture }[scale = 0.6]\ small\ coordinate(A) at(0, 0);\
    coordinate(B) at(1, 0);\
    coordinate(C) at(1, 1);\
    coordinate(D) at(0, 1);

    \
    draw[-{ latex }, draw = black, line width = 1.3 pt](0, 0) node[right] { $149 ^ {\ circ }
        $ }
    to++(70: 8) node[below = 6 pt, left = 4 pt] { $1$ }
    to++(178: 4) node[below right] { $81 ^ {\ circ }
        $ }
    to(-81: 2);\
    end { tikzpicture } }\
end { center }

\
textbf {\\ Question 46. }
What is the value of t ? \\\begin { center }\
resizebox { .5\ textwidth } {! } {\ begin { tikzpicture }[scale = 0.5]\ footnotesize\ draw[draw = black, line width = 1.1 pt](0, 0) node[below left] { $U$ }
    to coordinate(A) ++(-10: 8) node[below right] { $W$ }
    node[above = 10 pt, left = 8 pt] { $40 ^ {\ circ }
        $ }
    to coordinate(B) ++(130: 8) node[above] { $V$ }
    to(0, 0);\
    draw[Gray, line width = 1 pt](A) to + (70: 0.2) to + (-110: 0.2);\
    draw[Gray, line width = 1 pt](B) to + (30: 0.2) to + (-150: 0.2);\
    end { tikzpicture } }\
end { center }\
newpage\ textbf {\\ Question 47. }
The radius of a circle is 1. What is the length of an arc that subtends an angle of\(\frac {\ pi } { 5 }\) radians ? \\\begin { center }\
resizebox { .45\ textwidth } {! } {\ begin { tikzpicture }[scale = 0.5]\ footnotesize\ draw[draw = Gray, line width = 1 pt](0, 0) circle[radius = 5];\
    draw[draw = black, fill = black](0, 0) circle[radius = 3 pt];\
    draw[draw = black, line width = 1.2 pt](5, 0) coordinate(A) arc(0: 36: 5) coordinate(B);\
    draw[draw = black, line width = 1.2 pt](0, 0) node[above = 8 pt, right = 18 pt] { $\ frac {\ pi } { 5 }
        $ }
    to node[below] { $1$ }(A);\
    draw[draw = black, line width = 1.2 pt](0, 0) to(B);\
    end { tikzpicture } }\
end { center }

\
textbf {\\ Question 48. }
The diagram shows a triangle.\\\begin { center }\
resizebox { .3\ textwidth } {! } {\ begin { tikzpicture }[scale = 0.7]\ small\ draw[line width = 1 pt, draw = black](0, 0) node[above = 25 pt, right = 5 pt] { $39 ^ {\ circ }
        $ }
    to + (40: 5.5) node[left = 4 pt] { $104 ^ {\ circ }
        $ }
    to++(79: 8.5) node[right = 2 pt, below = 18 pt] { $w$ }
    to(0, 0);\
    end { tikzpicture } }\
end { center }
What is the value of w ? \newpage

\ textbf {\\ Question 49. }\(\triangle GHI\ sim\ triangle UVW.Find HI.\)\\\ begin { center }\
resizebox { .65\ textwidth } {! } {\ begin { tikzpicture }[scale = 0.8]\ small\ begin { scope }\
    draw[line width = 1.1 pt, draw = black](0, 0) node[below left] { $I$ }
    to + (15: 4) node[right] { $H$ }
    to++(75: 4) node[above] { $G$ }
    to node[left] { $8$ }(0, 0);\
    end { scope }\
    begin { scope }[xshift = 5.5 cm, yshift = 0.3 cm]\ draw[line width = 1.1 pt, draw = black](0, 0) node[below] { $W$ }
    to node[below] { $1$ } + (15: 3) node[right] { $V$ }
    to++(75: 3) node[above] { $U$ }
    to node[left] { $1$ }(0, 0);\
    end { scope }\
    end { tikzpicture } }\
end { center }\
textbf {\\ Question 50. }
Find m..\\\begin { center }\
resizebox { .5\ textwidth } {! } {\ begin { tikzpicture }[scale = 0.6]\ small\ draw[line width = 1 pt, color = Gray](0, 0) rectangle + (0.5, 0.5);\
    draw[line width = 1 pt, color = Gray](3, 0) rectangle + (0.5, 0.5);

    \
    draw[line width = 1.1 pt](0, 0) node[below] { $K$ }
    to(8, 0) node[below] { $H$ }
    to(0, 10) node[above] { $J$ }
    to node[left] { $8$ yd }(0, 0);\
    draw[line width = 1.1 pt](3, 0) node[below] { $G$ }
    to(8, 0) to node[above right] { $5$ yd }(3, 6.25) node[above right] { $I$ }
    to node[left] { $m$ }(3, 0);\
    node at(1.7, 9) { $5$ yd };\
    end { tikzpicture } }\
end { center }
Write your answer as a whole number or a decimal.Do not round.\\\newpage\ textbf {\\ Question 51. }\
begin { center }\
resizebox { .45\ textwidth } {! } {\ begin { tikzpicture }[scale = 0.6]\ draw[line width = 1 pt, color = Gray](7.5, 0) rectangle(8, 0.5);\
    draw[line width = 1.2 pt](0, 0) to node[below] { $32$ km }(8, 0) to node[right] { $24$ km }(8, 6) to node[above = 5 pt] { $c$ }(0, 0);\
    end { tikzpicture } }\
end { center }
What is the length of the hypotenuse ? .\\

\textbf {\\ Question 52. }
Find n.\\\begin { center }\
resizebox { .4\ textwidth } {! } {\ begin { tikzpicture }[scale = 0.6]\ small\ draw[line width = 1 pt, draw = Gray](0, 0) to++(150: 0.5) to++(-120: 0.5) to++(-30: 0.5) to(0, 0);\
    draw[line width = 1.2 pt, draw = black](0, 0) to node[above right] { $8\ sqrt { 3 }
        $ ft } + (150: 7.5) node[below = 25 pt, right = 16 pt] { $30 ^ {\ circ }
        $ }
    to++(-120: 4) node[above = 11 pt] { $60 ^ {\ circ }
        $ }
    to node[below right] { $n$ }(0, 0);\
    end { tikzpicture } }\
end { center }
Write your answer in simplest radical form.\newpage\ textbf {\\ Question 53. }\(\overline { TX }\
    parallel\ overline { UW }.Find TU\)\\\ begin { center }\
resizebox { .6\ textwidth } {! } {\ begin { tikzpicture }[scale = 0.9]\ small\ draw[line width = 0.9 pt, draw = orange](0, 0) node[above left] { $V$ }
    to node[above] { $48$ } + (0: 4) node[above] { $W$ }
    to + (-60: 2.9) node[below left] { $U$ }
    to node[below left] { $34$ }(0, 0);\
    draw[line width = 0.9 pt, draw = orange](0, 0) to + (0: 6) node[above right] { $X$ }
    to + (-60: 4.35) node[below left] { $T$ }
    to(0, 0);\
    node[above] at(5, 0) { $24$ };\
    end { tikzpicture } }\
end { center }\
textbf {\\ Question 54. }
This graph shows how the number of spools of thread that Elise has left is related to the number of shirts that she sews..\\\begin { center }\
resizebox { .65\ textwidth } {! } {\ begin { tikzpicture }[scale = 0.6]\ small\ foreach\ x in { 0, ..., 10 }\
    draw[line width = 0.15 pt](\x, 0) node[below] { $\ x$ }
    to(\x, 10);

    \
    foreach\ y in { 1, ..., 10 }\
    draw[line width = 0.15 pt](0, \y) node[left] { $\ y$ }
    to(10, \y);

    \
    draw[{ latex } - { latex }, line width = 1 pt](0, 10.5) to(0, 0) to(10.5, 0);\
    node at(5, 11) { Spools of thread remaining };\
    node at(5, -1.6) { Shirts sewn };\
    node[align = center, rotate = 90] at(-1.7, 5) { Spools of thread remaining };

    \
    draw[-{ latex }, line width = 1.8 pt, color = black](0, 7) to(4.8, 0);\
    end { tikzpicture } }\
end { center }
If Elise sews 2 shirts, how many spools of thread will she have left ? \\

    \newpage

\ textbf {\\ Question 55. }
Rewrite the following equation in slope - intercept form..\\\(y - 1 = 4(x - 1)\)\\
Write your answer using integers, proper fractions, and improper fractions in simplest form.\\\\

\textbf {\\ Question 56. }
What is\(m\ widehat { TU }\).\\\begin { center }\
resizebox { .5\ textwidth } {! } {\ begin { tikzpicture }[scale = 0.6]\ small\ draw[draw = black, line width = 1 pt](0, 0) circle[radius = 5];\
    draw[draw = Gray, line width = 1 pt](0, 0) to(20: 5) node[above right] { $U$ };\
    draw[draw = Gray, line width = 1 pt](0, 0) to(155: 5) node[above left] { $V$ };\
    draw[draw = Gray, line width = 1 pt](0, 0) to(275: 5) node[below] { $T$ };

    \
    draw[draw = none](0, 0) to(100: 5) node[above] { $140$ };\
    draw[draw = none](0, 0) to(210: 5) node[below left] { $110$ };\
    draw[draw = black, fill = black](0, 0) circle[radius = 4 pt];\
    end { tikzpicture } }\
end { center }\
newpage\ textbf {\\ Question 57. }
The radius of a circle is 6 miles.What is the length of a 135° arc ? \\\begin { center }\
resizebox { .5\ textwidth } {! } {\ begin { tikzpicture }[scale = 0.6]\ small\ draw[draw = Gray, line width = 1 pt](0, 0) circle[radius = 5];\
    draw[draw = black, line width = 1 pt](0, 0) to node[above right] { $r = 6 $ mi }(150: 5) arc(150: 220: 5) node[below left] { $135 ^ {\ circ }
        $ }
    arc(220: 285: 5);\
    draw[draw = black, line width = 1 pt](0, 0) to(285: 5);\
    draw[draw = black, fill = black](0, 0) circle[radius = 4 pt];\
    end { tikzpicture } }\
end { center }\
textbf {\\ Question 58. }\(\overleftrightarrow { VW }\) is tangent to $OU$.What is $UW$ ? \\\begin { center }\
resizebox { .5\ textwidth } {! } {\ begin { tikzpicture }[scale = 0.6]\ small\ draw[draw = Gray, line width = 1 pt](0, 0) circle[radius = 5];\
    draw[draw = black, line width = 1 pt](0, 0) to node[above left] { $16$ ft }++(55: 5) node[above right] { $V$ }
    to node[above right] { $12$ ft }++(-35: 3.75) node[above right] { $W$ }
    to(0, 0);\
    draw[draw = black, fill = black](0, 0) circle[radius = 4 pt];\
    end { tikzpicture } }\
end { center }\
newpage\ textbf {\\ Question 59. }
The radius of a circle is 3 miles.What is the area of a sector bounded by a 180° arc ? .\\\begin { center }\
resizebox { .5\ textwidth } {! } {\ begin { tikzpicture }[scale = 0.6]\ small\ draw[draw = Gray, line width = 1 pt](0, 0) circle[radius = 5];\
    draw[draw = black, line width = 1.2 pt, fill = black!50](0, 0) to node[above right] { $r = 3 $ mi }++(100: 5) arc(100: 190: 5) node[left] { $180 ^ {\ circ }
        $ }
    arc(190: 280: 5) to(0, 0);\
    draw[draw = black, fill = black](0, 0) circle[radius = 4 pt];\
    end { tikzpicture } }\
end { center }
Give the exact answer in simplest form.\\

\textbf {\\ Question 60. }
Find the tangent of\(\angle\) K.\\\begin { center }\
resizebox { .95\ textwidth } {! } {\ begin { tikzpicture }[scale = 0.6]\ small\ begin { scope }\
    draw[line width = 1 pt, draw = Gray](0, 0) to++(-30.5: 0.5) to++(-120.5: 0.5) to++(147.5: 0.5);

    \
    draw[line width = 0.5 pt, draw = black](0, 0) to++(-30.5: 7) to++(180: 8.125) coordinate(K);\
    draw[line width = 1 pt, draw = Gray](K) + (0.5, 0) arc(0: 59.5: 0.5);\
    draw[line width = 1.2 pt, draw = black](0, 0) node[above] { $J$ }
    to++(-30.5: 7) node[below right] { $I$ }
    to++(180: 8.125) node[below left] { $K$ }
    to(0, 0);\
    end { scope }\
    begin { scope }[yshift = -0.5 cm, xshift = 10 cm, scale = 0.7]\ draw[line width = 1 pt, draw = Gray](0, 0) to++(-30.5: 0.5) to++(-120.5: 0.5) to++(147.5: 0.5);

    \
    draw[line width = 0.5 pt, draw = black](0, 0) to++(-30.5: 7) to++(180: 8.125) coordinate(K);\
    draw[line width = 1 pt, draw = Gray](K) + (0.5, 0) arc(0: 59.5: 0.5);\
    draw[line width = 1.2 pt, draw = black](0, 0) node[above] { $G$ }
    to node[above right] { $56$ }++(-30.5: 7) node[below right] { $H$ }
    to node[below] { $65$ }++(180: 8.125) node[below left] { $F$ }
    to node[above left] { $33$ }(0, 0);\
    end { scope }\
    end { tikzpicture } }\
end { center }\
newpage\ textbf {\\ Question 61. }
Find\(m\ angle C\).\\\begin { center }\
resizebox { .5\ textwidth } {! } {\ begin { tikzpicture }[scale = 0.6]\ small\ draw[line width = 1 pt, draw = Gray](0, 0) to++(-50: 0.5) to++(-145: 0.5) to++(130: 0.51);

    \
    draw[line width = 1.2 pt, draw = Black](0, 0) node[above] { $D$ }
    to++(-50: 4.3) node[below right] { $E$ }
    to node[below] { $2\ sqrt { 19 }
        $ }++(180: 7.5) node[below left] { $C$ }
    to node[above left] { $\ sqrt { 57 }
        $ }(0, 0);\
    end { tikzpicture } }\
end { center }\
textbf {\\ Question 62. }
$\ cos(x) = -0.1 $.What is\(\sin(90 ^ {\ circ } - x)\) ? \\

    \textbf {\\ Question 63. }
What is the area of the shaded region ? \\\begin { center }\
resizebox { .5\ textwidth } {! } {\ begin { tikzpicture }[scale = 0.8]\ small\ draw[draw = black, line width = 1 pt, fill = Gray!60](0, 0) to node[below] { $37$ km } + (7, 0) to++(0, 8) to node[left] { $40$ km }(0, 0);\
    draw[draw = black, line width = 1 pt, fill = white](0.5, 1.2) to node[above] { $22$ km } + (4, 0) to++(0, 5) to node[right] { $23$ km }(0.5, 1.2);\
    end { tikzpicture } }\
end { center }
Write your answer as a whole number or a decimal rounded to the nearest hundredth.\newpage\ textbf {\\ Question 64. }
What is the volume ? .\\\begin { center }\
resizebox { .45\ textwidth } {! } {\ begin { tikzpicture }[scale = 0.45]\ small\ draw[draw = black, fill = black!20, line width = 1.2 pt](0, 0) to++(30: 5) to++(150: 5) to++(210: 5) to++(-30: 5);

    \
    draw[draw = black, fill = black!50, line width = 1.2 pt](0, 0) to++(150: 5) to++(-90: 5) to node[below left] { $9$ ft }++(-30: 5) to++(90: 5);

    \
    draw[draw = black, fill = black!80, line width = 1.2 pt](0, 0) to++(30: 5) to node[right] { $9$ ft }++(-90: 5) to node[below right] { $9$ ft }++(-150: 5) to++(90: 5);\
    end { tikzpicture } }\
end { center }\
textbf {\\ Question 65. }
What is the area of this figure ? \\\begin { center }\
resizebox { .42\ textwidth } {! } {\ begin { tikzpicture }[scale = 0.5]\ small\ draw[draw = black, line width = 1 pt, fill = black!40](0, 0) to node[below] { $4$ mm }++(4, 0) to node[right] { $17$ mm }++(0, 17) to node[above] { $6$ mm }++(-6, 0) to node[left] { $8$ mm }++(0, -8) to++(2, 0) node[below left] { $2$ mm }
    to node[left] { $9$ mm }(0, 0);\
    end { tikzpicture } }\
end { center }\
textbf {\\ Question 66. }
Lillian has the following data: ? \\\colorbox { lightgray } { n 58 66 61 59 60 61 }\\
If the median is 60, which number could n be ? \\

    \newpage\ textbf {\\ Question 67. }
Does this scatter plot show a positive trend, a negative trend, or no trend ? \\\begin { center }\
resizebox { .65\ textwidth } {! } {\ begin { tikzpicture }[scale = 0.6]\ small\ foreach\ x in { 20, 40, 60, 80, 100 }\
    draw[line width = 0.15 pt]({\ x / 10 }, 0) node[below] { $\ x$ }
    to({\ x / 10 }, 10);\
    foreach\ x in { 10, 30, 50, 70, 90 }\
    draw[line width = 0.15 pt]({\ x / 10 }, 0) to({\ x / 10 }, 10);

    \
    foreach\ y in { 20, 40, 60, 80, 100 }\
    draw[line width = 0.15 pt](0, {\ y / 10 }) node[left] { $\ y$ }
    to(10, {\ y / 10 });\
    foreach\ y in { 10, 30, 50, 70, 90 }\
    draw[line width = 0.15 pt](0, {\ y / 10 }) to(10, {\ y / 10 });

    \
    draw[{ latex } - { latex }, line width = 1 pt](0, 10.5) node[above] { $y$ }
    to(0, 0) node[below left] { $0$ }
    to(10.5, 0) node[right] { $x$ };

    \
    draw[draw = black, fill = black!60](4.0, 2.2) circle[radius = 3 pt];\
    draw[draw = black, fill = black!60](4.8, 3.2) circle[radius = 3 pt];\
    draw[draw = black, fill = black!60](4.9, 1.2) circle[radius = 3 pt];\
    draw[draw = black, fill = black!60](5.1, 1.0) circle[radius = 3 pt];\
    draw[draw = black, fill = black!60](5.2, 0.4) circle[radius = 3 pt];\
    draw[draw = black, fill = black!60](5.3, 3.1) circle[radius = 3 pt];\
    draw[draw = black, fill = black!60](5.9, 5.9) circle[radius = 3 pt];\
    draw[draw = black, fill = black!60](6.0, 0.2) circle[radius = 3 pt];\
    draw[draw = black, fill = black!60](6.0, 3.2) circle[radius = 3 pt];\
    draw[draw = black, fill = black!60](6.3, 8.0) circle[radius = 3 pt];\
    draw[draw = black, fill = black!60](6.6, 6.1) circle[radius = 3 pt];\
    draw[draw = black, fill = black!60](6.7, 2.9) circle[radius = 3 pt];\
    draw[draw = black, fill = black!60](6.9, 1.9) circle[radius = 3 pt];\
    draw[draw = black, fill = black!60](7.4, 4.4) circle[radius = 3 pt];\
    draw[draw = black, fill = black!60](7.6, 6.1) circle[radius = 3 pt];\
    draw[draw = black, fill = black!60](7.6, 9.0) circle[radius = 3 pt];\
    draw[draw = black, fill = black!60](7.8, 7.9) circle[radius = 3 pt];\
    draw[draw = black, fill = black!60](7.9, 6.5) circle[radius = 3 pt];\
    draw[draw = black, fill = black!60](8.2, 6.3) circle[radius = 3 pt];\
    draw[draw = black, fill = black!60](8.3, 5.8) circle[radius = 3 pt];\
    draw[draw = black, fill = black!60](8.5, 7.6) circle[radius = 3 pt];\
    draw[draw = black, fill = black!60](8.7, 8.9) circle[radius = 3 pt];\
    draw[draw = black, fill = black!60](9.1, 8.4) circle[radius = 3 pt];\
    end { tikzpicture } }\
end { center }\
textbf {\\ Question 68. }
What is the equation of the trend line in the scatter plot ? \\\begin { center }\
resizebox { .65\ textwidth } {! } {\ begin { tikzpicture }[scale = 0.6]\ small\ foreach\ x in { 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10 }\
    draw[line width = 0.15 pt](\x, 0) node[below] { $\ x$ }
    to(\x, 10.5);

    \
    foreach\ y in { 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10 }\
    draw[line width = 0.15 pt](0, \y) node[left] { $\ y$ }
    to(10.5, \y);

    \
    draw[{ latex } - { latex }, line width = 1 pt](0, 10.5) node[above] { $y$ }
    to(0, 0) to(10.5, 0) node[right] { $x$ };

    \
    draw[draw = black, fill = black!60](0.5, 6.0) circle[radius = 3 pt];\
    draw[draw = black, fill = black!60](0.6, 3.3) circle[radius = 3 pt];\
    draw[draw = black, fill = black!60](0.9, 6.5) circle[radius = 3 pt];\
    draw[draw = black, fill = black!60](1.1, 4.0) circle[radius = 3 pt];\
    draw[draw = black, fill = black!60](1.5, 6.6) circle[radius = 3 pt];\
    draw[draw = black, fill = black!60](1.8, 4.9) circle[radius = 3 pt];\
    draw[draw = black, fill = black!60](1.9, 4.8) circle[radius = 3 pt];\
    draw[draw = black, fill = black!60](2.3, 7.2) circle[radius = 3 pt];\
    draw[draw = black, fill = black!60](3.1, 6.1) circle[radius = 3 pt];\
    draw[draw = black, fill = black!60](3.9, 8.2) circle[radius = 3 pt];\
    draw[draw = black, fill = black!60](4.1, 5.6) circle[radius = 3 pt];\
    draw[draw = black, fill = black!60](4.5, 6.2) circle[radius = 3 pt];\
    draw[draw = black, fill = black!60](4.8, 8.1) circle[radius = 3 pt];\
    draw[draw = black, fill = black!60](4.9, 8.2) circle[radius = 3 pt];\
    draw[draw = black, fill = black!60](5.1, 6.8) circle[radius = 3 pt];\
    draw[draw = black, fill = black!60](5.8, 9.2) circle[radius = 3 pt];\
    draw[draw = black, fill = black!60](6.2, 8.7) circle[radius = 3 pt];\
    draw[draw = black, fill = black!60](6.4, 8.2) circle[radius = 3 pt];\
    draw[draw = black, fill = black!60](6.9, 9.0) circle[radius = 3 pt];\
    draw[draw = black, fill = black!60](8.0, 8.9) circle[radius = 3 pt];

    \
    draw[{ latex } - { latex }, line width = 1 pt](-0.5, 3.58) to(7.8, 10.5);\
    draw[draw = black, fill = black](0, 4) circle[radius = 4 pt];\
    draw[draw = black, fill = black](6, 9) circle[radius = 4 pt];\
    end { tikzpicture } }\
end { center }
What is the equation of the trend line in the scatter plot ?
    Use the two yellow points to write the equation in slope - intercept form.Write any coefficients as integers, proper fractions, or improper fractions in simplest form.\\\newpage\ textbf {\\ Question 69. }
Ashley surveyed the 37 book critics who read and reviewed a new novel.\\
Is this sample of the readers of the new novel likely to be biased ? ? \\

    \textbf {\\ Question 70. }
A wilderness retail store asked a consulting company to do an analysis of their hiking shoe customers.The consulting company gathered data from each customer that purchased hiking shoes, and recorded the shoe brand and the customer 's level of happiness.\\\
arrayrulecolor { gray }\
renewcommand {\ arraystretch } { 2 }\
begin { center }\
begin { tabular } { l | c | c | } &
\cellcolor { black!75 }\
color { white }\
bfseries Displeased & \cellcolor { black!75 }\
color { white }\
bfseries Pleased\\\ hline\ cellcolor { black!75 }\
color { white }\
bfseries A Footlong shoe & 4 & 6\\\ hline\ cellcolor { black!75 }\
color { white }\
bfseries A Toes Knows shoe & 5 & 5\\\ hline\ end { tabular }\
end { center }
What is the probability that a randomly selected customer purchased a Footlong shoe and is pleased ? \\

    \newpage %
    Answers are started from Here\ begin { center }\
textbf {\ Huge Answers }\\\
end { center }

\
textbf {\\ Answer 1. }
32\\\ textbf {\\ Answer 2. }
7\\\ textbf {\\ Answer 3. }
71\\\ textbf {\\ Answer 4. }
50\ % \\\textbf {\\ Answer 5. }
48.21\\\ textbf {\\ Answer 6. }
8.61\\\ textbf {\\ Answer 7. }
Linear\\\ textbf {\\ Answer 8. }
Linear\\\ textbf {\\ Answer 9. }\(6\ sqrt { 2 } - \sqrt { 6 }\)\\\ textbf {\\ Answer 10. }
2\\\ textbf {\\ Answer 11. }
18.0 milligrams\\\ textbf {\\ Answer 12. }\((8 s) ^ {\ frac { 1 } { 2 } }\)\\\ textbf {\\ Answer 13. }\(-2 x - 6\)\\\ textbf {\\ Answer 14. }
10\\\ textbf {\\ Answer 15. }
3\\\ textbf {\\ Answer 16. }
32\\\ textbf {\\ Answer 17. }\(y = x + 51\)\\\ textbf {\\ Answer 18. }
8\\\ textbf {\\ Answer 19. } - 2\\\ textbf {\\ Answer 20. }\(\frac { C } { 2\ pi }\)\\\ textbf {\\ Answer 21. }\(u\ geq 2\)\\\ textbf {\\ Answer 22. }
BLANK To be filled by you\\\ textbf {\\ Answer 23. }\(2 x + 4 y\ geq 2500\)\\\ textbf {\\ Answer 24. }
1\\\ textbf {\\ Answer 25. }
64\\\ textbf {\\ Answer 26. }
In first graph\(f(x) = -x ^ { 2 } - 10 x - 20\)\ quad In second graph\(g(x) = x ^ { 2 } + 2 x - 6\)\\\ textbf {\\ Answer 27. }
2, -4\\\ textbf {\\ Answer 28. }
BLANK To be filled by you\\\ textbf {\\ Answer 29. }
0\\\ textbf {\\ Answer 30. }
One real Solution\\\ textbf {\\ Answer 31. }
m = 0 and m = 3\\\ textbf {\\ Answer 32. } - 6, -2\\\ textbf {\\ Answer 33. }\((2 w + 5)(w + 2)\)\\\ textbf {\\ Answer 34. }\(2 a ^ { 2 } - 8\)\\\ textbf {\\ Answer 35. }\(9 k ^ { 2 } - 1\)\\\ textbf {\\ Answer 36. }\(12 f ^ { 3 } + 24 f\)\\\ textbf {\\ Answer 37. }\(x ^ { 2 } - 7 x + 12\)\\\ textbf {\\ Answer 38. }
In first graph\(g(x) = 2 x ^ { 2 } + 16 x + 32\) and in second graph\(f(x) = -x ^ { 2 } - 6\)\\\ textbf {\\ Answer 39. } - 7, 4\\\ textbf {\\ Answer 40. }
Infinitely many solutions\\\ textbf {\\ Answer 41. }
one solution\\\ textbf {\\ Answer 42. }
A body sculpting workout burns 56 calories and a yoga workout burns 77 calories.\\\textbf {\\ Answer 43. }(16, 508)(-3, -62)\\\ textbf {\\ Answer 44. }
136\\\ textbf {\\ Answer 45. }
68\\\ textbf {\\ Answer 46. }
70\\\ textbf {\\ Answer 47. }\(\frac {\ pi } { 5 }\)\\\ textbf {\\ Answer 48. }
37\\\ textbf {\\ Answer 49. }
8\\\ textbf {\\ Answer 50. }
4\\\ textbf {\\ Answer 51. }
40\\\ textbf {\\ Answer 52. }
8\\\ textbf {\\ Answer 53. }
17\\\ textbf {\\ Answer 54. }
4 spools of thread\\\ textbf {\\ Answer 55. }\(y = 4 x - 3\)\\\ textbf {\\ Answer 56. }
110\\\ textbf {\\ Answer 57. }\(\dfrac { 9 } { 2\ pi }\)\\\ textbf {\\ Answer 58. }
20\\\ textbf {\\ Answer 59. }\(\frac { 9 } { 2 }\
    pi\)\\\ textbf {\\ Answer 60. }\(\frac { 56 } { 33 }\)\\\ textbf {\\ Answer 61. }
30\\\ textbf {\\ Answer 62. } - 0.1\\\ textbf {\\ Answer 63. }
487\\\ textbf {\\ Answer 64. }
729\\\ textbf {\\ Answer 65. }
84\\\ textbf {\\ Answer 66. }
58\\\ textbf {\\ Answer 67. }
Positive Trend\\\ textbf {\\ Answer 68. }\(y = \frac { 5 } { 6 }
    x + 4\)\\\ textbf {\\ Answer 69. }
yes\\\ textbf {\\ Answer 70. }\(\frac { 3 } { 10 }\)\\

\ end { document }